<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Admin Routes
Route::get('/admindashboard', function () {  
    return view('admin.pages.dashboard');
})->name('admin.dashboard');

Route::get('/lead', function () {
    return view('admin.pages.lead');
});

// admin customer
Route::get('/customerprofilelist', function () {
    return view('admin.pages.customerprofilelist');
});

Route::get('/profileupdateapproval', function () {
    return view('admin.pages.profileupdateapproval');
});

Route::get('/paymentdetailsapproval', function () {
    return view('admin.pages.paymentdetailsapproval');
});

// admin vendor
Route::get('/vendorprofilelist', function () {
    return view('admin.pages.vendorprofilelist');
});


Route::get('/vendorapproval', function () {
    return view('admin.pages.vendorapproval');
});

Route::get('/enterpriseandpremiumvendor', function () {
    return view('admin.pages.enterpriseandpremiumvendor');
});

// Products

//////////////////// vendor

Route::get('/vendorproductlist', function () {
    return view('admin.pages.vendorproductlist');
});

Route::get('/vendorproductentryapproval', function () {
    return view('admin.pages.vendorproductentryapproval');
});

Route::get('/vendorproductupdateapproval', function () {
    return view('admin.pages.vendorproductupdateapproval');
});

Route::get('/vendorproductdeleteapproval', function () {
    return view('admin.pages.vendorproductdeleteapproval');
});

//////////////////// admin

Route::get('/adminproductlist', function () {
    return view('admin.pages.adminproductlist');
});


Route::post('/adminproductlist/categorycreation', 'CategoryController@saveCategory')->name('admin.pages.adminproductlist.categorycreation');
Route::get('/adminproductlist/getcategories', 'CategoryController@getCategories')->name('admin.pages.adminproductlist.getcategories');

Route::post('/adminproductlist/subcategorycreation','SubCategoryController@createSubCategory')->name('admin.pages.adminproductlist.subcategorycreation');
Route::get('/adminproductlist/getsubcategories/{categoryid}','SubCategoryController@getSubCategories')->name('admin.pages.adminproductlist.getsubcategories');
// OPerations

//////////////////////////////Enquiry

Route::get('/vendorenquiry', function () {
      return view('admin.pages.vendorenquiry');
});

Route::get('/customerenquiry', function () {
      return view('admin.pages.customerenquiry');
});

Route::get('/enquirymanagement', function () {
      return view('admin.pages.enquirymanagement');
});

Route::get('/enquirycancellation', function () {
      return view('admin.pages.enquirycancellation');
});


Route::get('/enquiryupdateapprove', function () {
      return view('admin.pages.enquiryupdateapprove');
});


//////////////////////////////// Quotation
Route::get('/vendorquotations', function () {
      return view('admin.pages.vendorquotations');
})->name('admin.vendorquotations');

Route::get('/vendoraskforrevisequotation',function(){
    return view('admin.pages.askforrevisequotation');
})->name('admin.vendor.askforrevisequatation');

Route::get('/vendorquotationcheckandforword',function(){
    return view('admin.pages.vendorquatationcheckforword');
})->name('admin.vendor.quatationcheckforword');


/*purchase order list */

Route::get('/customerpurchaseorderlist',function(){

    return view('admin.pages.customerpolist');

})->name('admin.customer.polist');

Route::get('/vendorpoupdateapproval',function(){

    return view('admin.pages.vendorpoupdateapproval');

})->name('admin.vendor.poupdateapproval');

Route::get('/pomanagement',function(){
    return view('admin.pages.pomanagement');
})->name('admin.pomanage');

Route::get('/livepo',function(){
    return view('admin.pages.polivemanagement');
})->name('admin.livepo');

Route::get('/wolist',function(){
    return view('admin.pages.wolist');
})->name('admin.wolist');

Route::get('/vendorwoapproval',function(){
    return view('admin.pages.vendorwoapproval');
})->name('admin.wolistvendor');


Route::get('/livewo',function(){
    return view('admin.pages.wolive');
})->name('admin.woliveprocess');

Route::get('/vendorwoissue',function(){
    return view('admin.pages.vendorwoissue');
})->name('admin.woissue');



//payments

Route::get('/customerbankdetails',function(){
    return view('admin.pages.customerbankdetailslist');
})->name('customer.bankdetails');

Route::get('/customerPOpaymenthistory',function(){
    return view('admin.pages.customerlivepopaymenthistory');
})->name('customer.livepopayhistory');

Route::get('/customerpaymenthistory',function(){
    return view('admin.pages.customerpaymenthistory');
})->name('customer.paymenthistory');

Route::get('/customersubcriptionpaymentactivity',function(){
    return view('admin.pages.subscriptionwisepaymentactivity');
})->name('customer.subpayactivity');

Route::get('/customerpaymentissuee',function(){
    return view('admin.pages.customerpaymentrelatedissue');
})->name('customer.payissue');





Route::get('/vendorbankdetails',function(){
    return view('admin.pages.vendorbankdetailslist');
})->name('vendor.bankdetails');

Route::get('/vendorpaymentactivity',function(){
    return view('admin.pages.vendorpaymentactivity');
})->name('vendor.paymentsactivity');

Route::get('/vendorshipmentpaymentactivity',function(){
    return view('admin.pages.vendorshipmentpaymentactivity');
})->name('vendor.shipmentpaymentsactivity');


Route::get('/vendorpaymentgatewayactivity',function(){
    return view('admin.pages.vendorpaymentgatewayactivity');
})->name('vendor.vendorpaymentgatewayactivity');


//support

Route::get('/supportissue',function(){
    return view('admin.pages.enquiryrelatedissue');
})->name('admin.enquiryrelatedissue');

Route::get('/supportquotissue',function(){
    return view('admin.pages.qutationrelatedissue');
})->name('admin.quotationrelatedissue');


Route::get('/porelatedissue',function(){
    return view('admin.pages.porelatedissue');
})->name('admin.porelatedissue');

Route::get('/worelatedissue',function(){
    return view('admin.pages.worelatedissue');
})->name('admin.worelatedissue');

Route::get('/shipmentrelatedissue',function(){
    return view('admin.pages.shipmentrelatedissue');
})->name('admin.shipmentrelatedissue');


Route::get('/transactionrelatedissue',function(){
    return view('admin.pages.transactionrelatredissue');
})->name('admin.transactionrelatedissue');




// Admin Routes ends

// customer RouteS

// Route::get('/customerdashboard', function () {
//     return view('customer.pages.customerdashboard');
// });

Route::get('/customerregistration', function () {
    return view('customer.pages.customerregistrationdummypage');
})->name('customer.registration');

Route::post('/customerregistrationstore', 'CustomerLeadController@store')->name('customer.registration.store');

Route::get('/customereemailverification/{remembertoken}', 'CustomerLeadController@EmailVerify')->name('customer.email.verifiction');


Route::get('/customerlogin', function () {
    return view('customer.pages.customerlogin');
})->name('customer.login');

Route::get('/customerprofile', function () {
    return view('customer.pages.customerprofile');
})->name('customer.profile');

Route::get('/customerpricingplan', function () {
    return view('customer.pages.customerpricingplan');
})->name('customer.subscription');

Route::get('/customertransactionhistory', function () {
    return view('customer.pages.customertransactionhistory');
})->name('customer.transaction.history');

Route::get('/customertransaction', function () {
    return view('customer.pages.customertransaction');
})->name('customer.transaction');

Route::get('/customerorderhistory', function () {
    return view('customer.pages.customerorderhistory');
})->name('customer.order.history');

Route::get('/customerorder', function () {
    return view('customer.pages.customerorder');
})->name('customer.order');

Route::get('/customerenquiry', function () {
    return view('customer.pages.customerenquiry');
})->name('customer.enquiry');

Route::get('/customerenquiryhistory', function () {
    return view('customer.pages.customerenquiryhistory');
})->name('customer.enquiry.history');


Route::get('/customerproductsearch', function () {
    return view('customer.pages.customerproductsearch');
})->name('customer.productsearch');

Route::get('/customerpayment', function () {
    return view('customer.pages.customerpayment');
})->name('customer.payment');

Route::get('/customerfeaturedvendors', function () {
    return view('customer.pages.customerfeaturedvendors');
})->name('customer.featuredvendors');

Route::get('/customerpremiumvendorprofile', function () {
    return view('customer.pages.customerpremiumvendorprofile');
})->name('customer.premiumvendor.profile');

Route::get('/customerenterprisevendorprofile', function () {
    return view('customer.pages.customerenterprisevendorprofile');
})->name('customer.enterprisevendor.profile');

Route::get('/customerinbox', function () {
    return view('customer.pages.customerinbox');
})->name('customer.inbox');

// customer route end

// vendor RouteS
Route::get('/vendordashboard', function () {
    return view('vendor.pages.vendordashboard');
});

Route::get('/vendorprofile', function () {
    return view('vendor.pages.vendorprofile');
})->name('vendor.profile');

Route::get('/vendororder', function () {
    return view('vendor.pages.vendororder');
})->name('vendor.order');


Route::get('/vendorpricingplan', function () {
    return view('vendor.pages.vendorpricingplan');
})->name('vendor.subscription');

Route::get('/vendortransactionhistory', function () {
    return view('vendor.pages.vendortransactionhistory');
})->name('vendor.transaction.history');

Route::get('/vendortransaction', function () {
    return view('vendor.pages.vendortransaction');
})->name('vendor.transaction');

Route::get('/vendororderhistory', function () {
    return view('vendor.pages.vendororderhistory');
})->name('vendor.order.history');
//
// Route::get('/customerorder', function () {
//     return view('customer.pages.customerorder');
// })->name('customer.order');
//
Route::get('/vendorenquiry', function () {
    return view('vendor.pages.vendorenquery');
})->name('vendor.enquiry');

Route::get('/vendorenquiryhistory', function () {
    return view('vendor.pages.vendorenquiryhistory');
})->name('vendor.enquiry.history');

//
Route::get('/vendorproductsearch', function () {
    return view('vendor.pages.vendorproducktsearch');
})->name('vendor.productsearch');

Route::get('/vendorpayment', function () {
    return view('vendor.pages.vendorpayment');
})->name('vendor.payment');



// Route::get('/customerfeaturedvendors', function () {
//     return view('customer.pages.customerfeaturedvendors');
// })->name('customer.featuredvendors');
//
// Route::get('/customerpremiumvendorprofile', function () {
//     return view('customer.pages.customerpremiumvendorprofile');
// })->name('customer.premiumvendor.profile');
//
// Route::get('/customerenterprisevendorprofile', function () {
//     return view('customer.pages.customerenterprisevendorprofile');
// })->name('customer.enterprisevendor.profile');
// vendor route end


/*customer data insert*/
Route::get('/insert/customerdata','CustomerController@insertCustomerInformation');

/*customer data insert useing loop*/
Route::get('/insert/customer/loop','CustomerController@insertCustomerInformationLoop');

/*vendor data insert */
Route::get('/insert/vendordata','VendorController@insertVendorInformation');

/*subcatagory creation*/

// Route::get('/insert/subcatagory','SubCatagoryController@createSubCatagory');

Route::get('/selector', function () {
    return view('admin.pages.categoryselector');
});

Route::post('/adminproductlist/productcreation','ProductController@createProduct')->name('admin.pages.adminproductlist.productcreation');
// Route::get('/categoryproducttable', 'CategoryProductTableController@createTable');
Route::get('/categoryproducttable', function () {
    return view('admin.pages.categoryproducttable');
});

Route::post('/vendorproductsearch/vendorproductcreation', 'TempVendorProductController@createProduct')->name('vendor.productsearch.vendorproductcreation');

// modal to modal route
Route::get('vendorproductrejectionassurance', 'TempVendorProductController@update')->name('admin.pages.modals.vendorproductrejectionassurance');
Route::get('vendorproductrejection');