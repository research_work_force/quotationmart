@extends('vendor.layouts.home')

@section('content')

<!-- ============================================================== -->
          <!-- Start right Content here -->
          <!-- ============================================================== -->
          <div class="main-content">

              <div class="page-content">



                  <!-- start page title -->
                  <div class="row">
                      <div class="col-12">
                          <div class="page-title-box d-flex align-items-center justify-content-between">
                              <h4 class="page-title mb-0 font-size-18">Vendor Enquiry</h4>

                              <div class="page-title-right">
                                  <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Vendor</a></li>
                                    <li class="breadcrumb-item active">Vendor Enquiry </li>
                                  </ol>
                              </div>

                          </div>
                      </div>
                  </div>
                  <!-- end page title -->

                  <div class="row mb-4">
                    <div class="col-4">

                    </div>
                    <div class="col-5">

                    </div>

                      <div class="col-3">
                        <a href=" {{ route('vendor.enquiry.history') }}" type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                          View Enquiry History
                      </a>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xl-12">

                          <div class="row">
                              <div class="col-md-3">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="col-8">
                                                  <div>
                                                      <p class="text-muted font-weight-medium mt-1 mb-2"># of Enquiry</p>
                                                      <h4>25</h4>
                                                  </div>
                                              </div>

                                              <div class="col-4">
                                                  <div>
                                                      <!-- <div id="radial-chart-1"></div> -->
                                                  </div>
                                              </div>
                                          </div>

                                          <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.8% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                      </div>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="col-10">
                                                  <div>
                                                      <p class="text-muted font-weight-medium mt-1 mb-2">Avg. Enquiry / Month</p>
                                                      <h4>5</h4>
                                                  </div>
                                              </div>

                                              <div class="col-2">
                                                  <div>
                                                      <!-- <div id="radial-chart-1"></div> -->
                                                  </div>
                                              </div>
                                          </div>

                                          <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.8% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                      </div>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="col-10">
                                                  <div>
                                                      <p class="text-muted font-weight-medium mt-1 mb-2">Avg. Enquiry / yearly</p>
                                                      <h4>25</h4>
                                                  </div>
                                              </div>

                                              <div class="col-2">
                                                  <div>
                                                      <!-- <div id="radial-chart-1"></div> -->
                                                  </div>
                                              </div>
                                          </div>

                                          <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.8% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                      </div>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="col-10">
                                                  <div>
                                                      <p class="text-muted font-weight-medium mt-1 mb-2"># of Cancelled Order</p>
                                                      <h4>10</h4>
                                                  </div>
                                              </div>

                                              <div class="col-2">
                                                  <div>
                                                      <!-- <div id="radial-chart-2"></div> -->
                                                  </div>
                                              </div>
                                          </div>

                                          <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.6% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row">


                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-10">
                                                <div>
                                                    <p class="text-muted font-weight-medium mt-1 mb-2"># of Enquiry Convertion</p>
                                                    <h4>10</h4>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div>
                                                    <!-- <div id="radial-chart-2"></div> -->
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.6% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-10">
                                                <div>
                                                    <p class="text-muted font-weight-medium mt-1 mb-2"># of Enquiries in queue</p>
                                                    <h4>1</h4>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div>
                                                    <!-- <div id="radial-chart-1"></div> -->
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.8% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div>
                                                    <p class="text-muted font-weight-medium mt-1 mb-2">Last Enquiry Date</p>
                                                    <h4>02/05/2020</h4>
                                                </div>
                                            </div>


                                        </div>

                                        <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.6% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                    </div>
                                </div>
                            </div>

                          </div>



                      </div>
                      <div class="col-xl-12">
                          <div class="card">
                              <div class="card-body">
                                  <!-- <div class="float-right">
                                      <ul class="nav nav-pills">
                                          <li class="nav-item">
                                              <a class="nav-link" href="#">Week</a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link" href="#">Month</a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link active" href="#">Year</a>
                                          </li>
                                      </ul>
                                  </div> -->
                                  <h4 class="card-title mb-4"># of Enquiries</h4>

                                  <div id="mixed-chart-customer-enquiry" class="apex-charts"></div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- end row -->

                  <div class="row">



                      <div class="col-xl-4">
                          <div class="card">
                              <div class="card-body">
                                  <h4 class="card-title mb-4">Last Enquiry</h4>

                                  <div class="row">
                                      <div class="col-lg-12">
                                          <div>

                                        
                                              <p><b>Order Date</b> 02/05/2020</p>

                                              <p><b>Product Name</b> ABC indicator</p>


                                              <p class="mb-2"><b>Quantity</b>4</p>
                                              <h4>4</h4>
                                          </div>

                                          <div class="row">
                                              <div class="col-sm-6">
                                                  <div class="mt-3">
                                                      <p class="mb-2 text-truncate"><b>Offer submited</b></p>No
                                                      
                                                  </div>
                                              </div>
                                              <div class="col-sm-6">
                                                  <div class="mt-3">
                                                      <p class="mb-2 text-truncate"><b>Tracking ID</b></p>
                                                      <h5>STV1245632</h5>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="mt-4">
                                              <a href="#" class="btn btn-dark btn-technomart btn-sm">View more</a>
                                          </div>
                                      </div>

                                  </div>
                              </div>
                          </div>
                      </div>



                      <div class="col-xl-8">
                        <div class="card">
                                  <div class="card-body">
                                      <h4 class="card-title mb-4">Enquiry Conversion Graph</h4>

                                      <div id="line-chart-customer-enquiry" class="apex-charts"></div>
                                  </div>
                              </div>
                      </div>

                  </div>
                  <!-- end row -->

                  <!-- end row -->
              </div>
              <!-- End Page-content -->





@endsection
