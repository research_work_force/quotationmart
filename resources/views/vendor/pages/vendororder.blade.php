@extends('vendor.layouts.home')

@section('content')

<!-- ============================================================== -->
          <!-- Start right Content here -->
          <!-- ============================================================== -->
         <div class="main-content">

              <div class="page-content">



                  <!-- start page title -->
                  <div class="row">
                      <div class="col-12">
                          <div class="page-title-box d-flex align-items-center justify-content-between">
                              <h4 class="page-title mb-0 font-size-18">Vendor Order</h4>

                              <div class="page-title-right">
                                  <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Vendor</a></li>
                                    <li class="breadcrumb-item active">Vendor Order </li>
                                  </ol>
                              </div>

                          </div>
                      </div>
                  </div>
                  <!-- end page title -->

                  <div class="row mb-4">
                    <div class="col-4">

                    </div>
                    <div class="col-5">

                    </div>

                      <div class="col-3">
                        <a href="{{ route('vendor.order.history') }}" type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                          View Order History
                      </a>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-xl-12">

                          <div class="row">
                              <div class="col-md-3">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="col-8">
                                                  <div>
                                                      <p class="text-muted font-weight-medium mt-1 mb-2"># Order</p>
                                                      <h4>10</h4>
                                                  </div>
                                              </div>

                                              <div class="col-4">
                                                  <div>
                                                      <!-- <div id="radial-chart-1"></div> -->
                                                  </div>
                                              </div>
                                          </div>

                                          <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.8% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="col-8">
                                                  <div>
                                                      <p class="text-muted font-weight-medium mt-1 mb-2">Order Value</p>
                                                      <h4>₹ 32,695</h4>
                                                  </div>
                                              </div>

                                              <div class="col-4">
                                                  <div>
                                                      <!-- <div id="radial-chart-2"></div> -->
                                                  </div>
                                              </div>
                                          </div>

                                          <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.6% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="col-10">
                                                  <div>
                                                      <p class="text-muted font-weight-medium mt-1 mb-2">Avg. Order / Month</p>
                                                      <h4>2</h4>
                                                  </div>
                                              </div>

                                              <div class="col-2">
                                                  <div>
                                                      <!-- <div id="radial-chart-1"></div> -->
                                                  </div>
                                              </div>
                                          </div>

                                          <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.8% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                      </div>
                                  </div>
                              </div>

                              <div class="col-md-3">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="col-10">
                                                  <div>
                                                      <p class="text-muted font-weight-medium mt-1 mb-2">Avg. Order / yearly</p>
                                                      <h4>10</h4>
                                                  </div>
                                              </div>

                                              <div class="col-2">
                                                  <div>
                                                      <!-- <div id="radial-chart-1"></div> -->
                                                  </div>
                                              </div>
                                          </div>

                                          <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.8% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <div class="row">

                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-10">
                                                <div>
                                                    <p class="text-muted font-weight-medium mt-1 mb-2">Total Debited Value</p>
                                                    <h4>₹ 32,695</h4>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div>
                                                    <!-- <div id="radial-chart-2"></div> -->
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.6% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-10">
                                                <div>
                                                    <p class="text-muted font-weight-medium mt-1 mb-2"># of Order in Queue</p>
                                                    <h4>10</h4>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div>
                                                    <!-- <div id="radial-chart-2"></div> -->
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.6% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-10">
                                                <div>
                                                    <p class="text-muted font-weight-medium mt-1 mb-2"># Cancelled Order</p>
                                                    <h4>0</h4>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div>
                                                    <!-- <div id="radial-chart-1"></div> -->
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.8% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div>
                                                    <p class="text-muted font-weight-medium mt-1 mb-2"># Completed Orders</p>
                                                    <h4>8</h4>
                                                </div>
                                            </div>


                                        </div>

                                        <!-- <p class="mb-0"><span class="badge badge-soft-success mr-2"> 0.6% <i class="mdi mdi-arrow-up"></i> </span> From previous period</p> -->
                                    </div>
                                </div>
                            </div>

                          </div>



                      </div>
                      <div class="col-xl-12">
                          <div class="card">
                              <div class="card-body">
                                  <!-- <div class="float-right">
                                      <ul class="nav nav-pills">
                                          <li class="nav-item">
                                              <a class="nav-link" href="#">Week</a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link" href="#">Month</a>
                                          </li>
                                          <li class="nav-item">
                                              <a class="nav-link active" href="#">Year</a>
                                          </li>
                                      </ul>
                                  </div> -->
                                  <h4 class="card-title mb-4"># of Orders</h4>

                                  <div id="mixed-chart-customer-order" class="apex-charts"></div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- end row -->

                  <div class="row">



                      <div class="col-xl-4">
                          <div class="card">
                              <div class="card-body">
                                  <h4 class="card-title mb-4">Last Order</h4>

                                  <div class="row">
                                      <div class="col-lg-12">
                                          <div>

                                            <p> <b>Order ID</b> XYZ/ODR/2020/10</p>
                                              <p><b>Order Date</b> 02/05/2020</p>

                                              <p><b>Vendor Name</b> ABC Pvt. LTD.</p>


                                              <p class="mb-2"><b>Order Value</b></p>
                                              <h4>INR 4,00,000.00</h4>
                                          </div>

                                          <div class="row">
                                              <div class="col-sm-6">
                                                  <div class="mt-3">
                                                      <p class="mb-2 text-truncate"><b>Order Status</b></p>
                                                      <h5 class="d-inline-block align-middle mb-0">In Transit</h5>
                                                  </div>
                                              </div>
                                              <div class="col-sm-6">
                                                  <div class="mt-3">
                                                      <p class="mb-2 text-truncate"><b>Tracking ID</b></p>
                                                      <h5>AHHALD54</h5>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="mt-4">
                                              <a href="#" class="btn btn-dark btn-technomart btn-sm">View more</a>
                                          </div>
                                      </div>

                                  </div>
                              </div>
                          </div>
                      </div>



                      <div class="col-xl-8">
                        <div class="card">
                                  <div class="card-body">
                                      <h4 class="card-title mb-4">Order Value Graph</h4>

                                      <div id="line-chart-customer-order" class="apex-charts"></div>
                                  </div>
                              </div>
                      </div>

                  </div>
                  <!-- end row -->

                  <!-- end row -->
              </div>
              <!-- End Page-content -->






@endsection
