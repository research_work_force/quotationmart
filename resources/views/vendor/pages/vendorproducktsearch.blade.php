@extends('vendor.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Vendor Product Search</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Vendor</a></li>
                            <li class="breadcrumb-item active">Vendor Product Search</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">List of Products</h1>
                        <!-- <p class="card-title-desc">
                            “It is not the employer who pays the wages. Employers only handle the money. It is the customer who pays the wages.” – Henry Ford
                        </p> -->

                        <div class="row">
                                      <div class="col-md-3 mb-4">
                                        <select class="form-control" name="catagory">
                                          <option value="">catagory 1</option>
                                          <option value="">catagory 2</option>
                                        </select>
                                      </div>
                                      <div class="col-md-3 mb-4">
                                        <select class="form-control" name="catagory">
                                          <option value="">Sub Catagory 1</option>
                                          <option value="">Sub Catagory 2</option>
                                        </select>
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Product Name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                        <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                          <i class="bx bx-loader bx-search font-size-16 align-middle mr-2"></i> Find a Product
                                      </button>
                                      </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
          <div class="col-4">
              <!-- <h4>Total No of Enquiry: 25<h4> -->
          </div>
          <div class="col-5">
            <!-- <h4>  Total No of Offer Submitted: 5210 <h4> -->
          </div>

            <div class="col-3">
                <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;" data-toggle="modal" data-target=".vendorproductadd-modal-center">
                    New product entry
                </button>
            </div>
            @include('vendor.pages.modals.vendorproductaddmodal')
        </div>

        <!-- Second Card -->

                <div class="row mt-4">
                    <div class="col-lg-3">
                      <a href="#" >
                      <div class="card">
                          <div class="card-body">


                              <div class="row ">
                                  <div class="col-lg-12" align="center">
                                    <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                                      <div class="mt-4">
                                          <h5>Catagory Name</h5>
                                      </div>
                                  </div>

                              </div>
                          </div>
                      </div>
                    </a>
                  </div>

                  <div class="col-lg-3">
                    <a href="#" >
                    <div class="card">
                        <div class="card-body">


                            <div class="row ">
                                <div class="col-lg-12" align="center">
                                  <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                                    <div class="mt-4">
                                        <h5>Catagory Name</h5>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                  </a>
                </div>

                <div class="col-lg-3">
                  <a href="#" >
                  <div class="card">
                      <div class="card-body">


                          <div class="row ">
                              <div class="col-lg-12" align="center">
                                <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                                  <div class="mt-4">
                                      <h5>Catagory Name</h5>
                                  </div>
                              </div>

                          </div>
                      </div>
                  </div>
                </a>
              </div>

              <div class="col-lg-3">
                <a href="#" >
                <div class="card">
                    <div class="card-body">


                        <div class="row ">
                            <div class="col-lg-12" align="center">
                              <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                                <div class="mt-4">
                                    <h5>Catagory Name</h5>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
              </a>
            </div>

            <div class="col-lg-3">
              <a href="#" >
              <div class="card">
                  <div class="card-body">


                      <div class="row ">
                          <div class="col-lg-12" align="center">
                            <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                              <div class="mt-4">
                                  <h5>Catagory Name</h5>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
            </a>
          </div>
          <div class="col-lg-3">
            <a href="#" >
            <div class="card">
                <div class="card-body">


                    <div class="row ">
                        <div class="col-lg-12" align="center">
                          <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                            <div class="mt-4">
                                <h5>Catagory Name</h5>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3">
          <a href="#" >
          <div class="card">
              <div class="card-body">


                  <div class="row ">
                      <div class="col-lg-12" align="center">
                        <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                          <div class="mt-4">
                              <h5>Catagory Name</h5>
                          </div>
                      </div>

                  </div>
              </div>
          </div>
        </a>
      </div>
      <div class="col-lg-3">
        <a href="#" >
        <div class="card">
            <div class="card-body">


                <div class="row ">
                    <div class="col-lg-12" align="center">
                      <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                        <div class="mt-4">
                            <h5>Catagory Name</h5>
                        </div>
                    </div>

                </div>
            </div>
        </div>
      </a>
    </div>
                </div>

                <div class="row">

                  <div class="col-xl-12">
                      <div class="card">
                          <div class="card-body">
                              <h4 class="card-title mb-4">Last product</h4>

                              <div class="row">
                                  <div class="col-lg-12">


                                      <div class="row">
                                          <div class="col-sm-3">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Catagory Name</b></p>
                                                  <h5 class="d-inline-block align-middle mb-0">Industrial Lights</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-3">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Sub Catagory Name</b></p>
                                                  <h5>LED Flood Light</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-3">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Product Name</b></p>
                                                  <h5>VIN Make 90-Watt Flood Light</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-3">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Product listed as</b></p>
                                                  <h5>Resell</h5>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="mt-4">
                                          <a href="#" class="btn btn-dark btn-technomart btn-sm">Continue Search</a>
                                      </div>
                                  </div>

                              </div>
                          </div>
                      </div>
                  </div>

                </div>
        <!-- Second row end -->

    </div>
    <!-- End Page-content -->








@endsection
