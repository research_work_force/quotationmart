@extends('vendor.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Profile</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Vendor</a></li>
                            <li class="breadcrumb-item active">Profile</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- start row -->
        <div class="row">
            <div class="col-md-12 col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="profile-widgets py-3">

                            <div class="text-center">
                                <div class="">
                                    <img src="assets/images/users/avatar-2.jpg" alt="" class="avatar-lg mx-auto img-thumbnail rounded-circle">
                                    <div class="online-circle"><i class="fas fa-circle text-success"></i></div>
                                </div>

                                <div class="mt-3 ">
                                    <a href="#" class="text-dark font-weight-medium font-size-16">Vendor Name</a>
                                    <p class="text-body mt-1 mb-1">@username</p>

                                    <span class="badge badge-success">Pro</span>
                                    <span class="badge badge-danger">3o days left</span>
                                </div>

                                <div class="row mt-4 border border-left-0 border-right-0 p-3">
                                    <div class="col-md-12">
                                        <h6 class="text-muted">
                                        Contact Joined Since
                                    </h6>
                                        <h5 class="mb-0">02/06/2019</h5>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>




            </div>

            <div class="col-md-12 col-xl-9">


              <div class="card">
                  <div class="card-body">
                      <h5 class="card-title mb-3">Personal Information</h5>

                      <p class="card-title-desc">
                          Hi I'm Patrick Becker, been industry's standard dummy ultrices Cambridge.
                      </p>
                      <div class="row">
                        <div class="col-md-4">

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Name </p>
                              <h6 class="">ABC Pvt. Ltd</h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Designation</p>
                              <h6 class="">Purchase Manager</h6>
                          </div>


                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company GST No</p>
                              <h6 class="">ABC123456789 </h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Pan No</p>
                              <h6 class="">PQRO23456</h6>
                          </div>

                        </div>

                        <div class="col-md-4">




                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Pan Photo</p>
                              <h6 class=""><a href="javascript: void(0);">View Pan Card</a></h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company MSME Registration No</p>
                              <h6 class="">khghgh565ljj</h6>
                          </div>
                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Nature of Company</p>
                              <h6 class="">Botling Manufacturing </h6>
                          </div>
                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Signature photo</p>
                              <h6 class=""><a href="javascript: void(0);">View Signature</a></h6>
                          </div>

                        </div>

                        <div class="col-md-4">



                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Address</p>
                              <h6 class="">2240 Denver Avenue
                                    Los Angeles, CA 90017</h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Visiting Card/ Id Card</p>
                              <h6 class=""><a href="javascript: void(0);">View Visiting ID Card</a></h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Logo</p>
                              <h6 class=""><img src="{{asset('assets/images/logo-dark.png')}}" alt="" width="202" height="40" class="mx-auto "></h6>
                          </div>


                        </div>



                      </div>

                      <div class="row">

                        <div class="col-md-4">
                          <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                             Update Profile
                        </button>

                        </div>
                        <div class="col-md-4">
                          <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                            Change Password
                        </button>

                        </div>
                        <div class="col-md-4">


                        <button class="btn btn-dark btn-technomart waves-effect waves-light " style="width:100%;" type="button" data-toggle="collapse" data-target="#collapseTal" aria-expanded="false" aria-controls="collapseExample">
                              Assign Users
                        </button>

                        </div>




                      </div>





                  </div>
              </div>


            </div>


 </div>

                         <div class="row collapse"  id="collapseTal">

                           <div class="col-md-12 col-xl-12">


                               <div class="card">

                                   <div class="card-body">
                                     <h5 class="card-title mb-3" align="center">Assign Users</h5>
                                       <!-- Nav tabs -->
                                       <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                           <li class="nav-item">
                                               <a class="nav-link active" data-toggle="tab" href="#newuser" role="tab">
                                                   <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                                   <span class="d-none d-sm-block">New User</span>
                                               </a>
                                           </li>
                                           <li class="nav-item">
                                               <a class="nav-link" data-toggle="tab" href="#manageuser" role="tab">
                                                   <span class="d-none d-sm-block">Manage User</span>
                                               </a>
                                           </li>

                                       </ul>

                                       <!-- Tab panes -->
                                       <div class="tab-content p-3 text-muted">


                                           <div class="tab-pane active" id="newuser" role="tabpanel">

                                               <div class="row mt-4">
                                                   <div class="col-md-6">
                                                       <div class="form-group">
                                                           <label for="firstname">Name of the User</label>
                                                           <input type="text" class="form-control" id="firstname" placeholder="Enter first name">
                                                       </div>
                                                   </div>
                                                   <div class="col-md-6">
                                                       <div class="form-group">
                                                           <label for="lastname">Role of the User</label>
                                                           <input type="text" class="form-control" id="lastname" placeholder="Enter last name">
                                                       </div>
                                                   </div>
                                                   <!-- end col -->
                                               </div>
                                               <div class="row mt-4">


                                                 <div class="col-md-4">
                                                     <div class="form-group">
                                                         <label for="firstname">Phone Number</label>
                                                         <input type="number" class="form-control" id="firstname" placeholder="Enter Phone Number">
                                                     </div>
                                                 </div>
                                                   <div class="col-md-4">
                                                       <div class="form-group">
                                                           <label for="lastname">Email ID</label>
                                                           <input type="text" class="form-control" id="lastname" placeholder="Enter last name">
                                                       </div>
                                                   </div>
                                                   <div class="col-md-4">
                                                       <div class="form-group">
                                                           <label for="firstname">Designation</label>
                                                           <input type="number" class="form-control" id="firstname" placeholder="Enter Phone Number">
                                                       </div>
                                                   </div>


                                                  <!-- end col -->
                                               </div>

                                               <div class="row mt-4">


                                                   <div class="col-md-3">
                                                     <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                                       Create New User
                                                   </button>
                                                   </div>

                                                  <!-- end col -->
                                               </div>




                                           </div>

                                           <div class="tab-pane" id="manageuser" role="tabpanel">


                                             <div class="row">

                                               <div class="col-md-6">
                                                 <p class="mb-2 card-heading-id" style="" align="center"><b>User ID:</b> asoaj56464</p>
                                                 <div class="row">
                                                    <div class="col-md-6">
                                                      <div class="mt-3">
                                                          <p class="font-size-12 text-muted mb-1">Name of The user</p>
                                                          <h6 class="">Aajajsja Jjahsajlsjjlja</h6>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="mt-3">
                                                          <p class="font-size-12 text-muted mb-1">Role of the User</p>
                                                          <h6 class="">Technical</h6>
                                                      </div>
                                                    </div>

                                                 </div>
                                                 <div class="row">
                                                    <div class="col-md-4">
                                                      <div class="mt-3">
                                                          <p class="font-size-12 text-muted mb-1">User Mobile No</p>
                                                          <h6 class="">9848293560</h6>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <div class="mt-3">
                                                          <p class="font-size-12 text-muted mb-1">User Email Id</p>
                                                          <h6 class="">ashamukul@xyz.in</h6>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <div class="mt-3">
                                                          <p class="font-size-12 text-muted mb-1">User Designation</p>
                                                          <h6 class="">Sr. Electrical Engineer</h6>
                                                      </div>
                                                    </div>

                                                 </div>
                                                      <!-- end col -->
                                               </div>

                                               <div class="col-md-6">
                                                 <p class="mb-2 card-heading-id" style="" align="center"><b>User ID:</b> asoaj56464</p>
                                                 <div class="row">
                                                    <div class="col-md-6">
                                                      <div class="mt-3">
                                                          <p class="font-size-12 text-muted mb-1">Name of The user</p>
                                                          <h6 class="">Aajajsja Jjahsajlsjjlja</h6>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                      <div class="mt-3">
                                                          <p class="font-size-12 text-muted mb-1">Role of the User</p>
                                                          <h6 class="">Technical</h6>
                                                      </div>
                                                    </div>

                                                 </div>
                                                 <div class="row">
                                                    <div class="col-md-4">
                                                      <div class="mt-3">
                                                          <p class="font-size-12 text-muted mb-1">User Mobile No</p>
                                                          <h6 class="">9848293560</h6>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <div class="mt-3">
                                                          <p class="font-size-12 text-muted mb-1">User Email Id</p>
                                                          <h6 class="">ashamukul@xyz.in</h6>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <div class="mt-3">
                                                          <p class="font-size-12 text-muted mb-1">User Designation</p>
                                                          <h6 class="">Sr. Electrical Engineer</h6>
                                                      </div>
                                                    </div>

                                                 </div>
                                                      <!-- end col -->
                                               </div>

                                             </div>





                                             <div class="row mt-4" align="center">

                                               <div class="col-md-6">
                                                 <button type="button" class="btn btn-warning  waves-effect waves-light">
                                                   Reassign
                                               </button>
                                               <button type="button" class="btn btn-success  waves-effect waves-light">
                                                 Edit
                                             </button>
                                             <button type="button" class="btn btn-danger  waves-effect waves-light">
                                               Delete
                                           </button>
                                               </div>
                                               <div class="col-md-6">
                                                 <button type="button" class="btn btn-warning  waves-effect waves-light">
                                                   Reassign
                                               </button>
                                               <button type="button" class="btn btn-success  waves-effect waves-light">
                                                 Edit
                                             </button>
                                             <button type="button" class="btn btn-danger  waves-effect waves-light">
                                               Delete
                                           </button>
                                               </div>
                                               </div>




                                                <!-- end col -->
                                             </div>
                                           </div>


                                       </div>

                                   </div>
                               </div>


                           </div>



<!-- end row -->

</div>








@endsection
