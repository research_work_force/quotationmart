<div class="modal fade vendorproductadd-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Create Vendor Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method='POST' action="{{ route('vendor.productsearch.vendorproductcreation') }}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-12">
                            @php
                            $categories = App\Http\Controllers\CategoryController::fetchCategories();
                            @endphp
                            <select class="form-control" name='categoryselector' required>
                                <option selected="" disabled="">--Choose catagory--</option>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}">
                                    {{ $category->categoryname }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
    
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <select name="subcategoryselector" class="form-control">
                                <option>--Select Sub-Category--</option>
                            </select>
    
                        </div>
                    </div>
    
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <input type="text" placeholder="product name" class="form-control" name="productname">
    
                        </div>
                    </div>
    
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <label>Technical Specification</label>
                            <textarea placeholder="Technical specifications" class="form-control" name="technicalspecs"> </textarea>
    
                        </div>
                    </div>
    
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <label>Product Description</label>
                            <textarea placeholder="product description" class="form-control" name="productdesc"> </textarea>
    
                        </div>
                    </div><br>
    
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <input type="text" placeholder="HSN code" class="form-control" name="hsncode">
    
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-12">
                            <input type="text" placeholder="Country of Origin" class="form-control" name="countryoforigin">
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-md-12">
                            <input type="text" placeholder="Manufactured By" class="form-control" name="mfdby">
                        </div>
                    </div>

                    <!-- <div class="row mt-2">
                        <div class="col-md-12">
                            <label>Product For</label>
                            <div class="form-check mb-2">
    
    
                                <input class="form-check-input" type="radio" name="productfor" id="exampleRadios1" value="directsell" checked>
                                <label class="form-check-label">
                                    Direct Sell
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="productfor" id="exampleRadios2" value="sell">
                                <label class="form-check-label">
                                    Resell
                                </label>
                            </div>
                        </div>
                    </div> -->
    
    
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <div>
                                <label>Choose image 1</label>
                                <input type="file" id="customFile" accept=".jpg,.jpeg,.png" name="productimg1">
                                <!-- <label  for="customFile">Choose image</label> -->
                            </div>
    
                        </div>
    
    
                    </div>
    
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <div>
                                <label>Choose image 2</label>
                                <input type="file" id="customFile" accept=".jpg,.jpeg,.png" name="productimg2">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <div>
                                <label>Choose image 3</label>
                                <input type="file" id="customFile" accept=".jpg,.jpeg,.png" name="productimg3">
                            </div>
    
                        </div>
    
    
                    </div>
    
                    <div class="row mt-2">
                        <div class="col-md-12">
                            <div>
                                <label>Choose image 4</label>
                                <input type="file" id="customFile" accept=".jpg,.jpeg,.png" name="productimg4">
                            </div>
    
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $('select[name="categoryselector"]').on('change', function() {
            $categoryid = $(this).val();
            if ($categoryid) {
                $.ajax({
                    url: '/adminproductlist/getsubcategories/' + $categoryid,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        console.log(data);

                        $('select[name="subcategoryselector"]').empty();

                        $.each(data, function(key, value) {
                            $('select[name="subcategoryselector"]').append('<option value="' + key + '">' + value + '</option>');

                        });
                    },
                    error: function(e) {
                        console.log(e);

                    },
                    complete: function() {
                        $('#loader').css("visibility", "hidden");
                    }

                });
            } else {
                $('select[name="subcategoryselector"]').empty();
            }
        })
    })
</script>
<!-- /.modal -->