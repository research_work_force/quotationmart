<!doctype html>
<html lang="en">

<head>
    <title> Vendor Dashboard | Questionmart</title>

    @include('vendor.includes.headerlinks')

</head> 

<body data-layout="detached" data-topbar="colored">

    <div class="container-fluid">
        <!-- Begin page -->
        <div id="layout-wrapper">

             @include('vendor.includes.navbarheader')
             @include('vendor.includes.sidebar')

             @yield('content')

             @include('vendor.includes.footer')
         </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

    </div>
    <!-- end container-fluid -->

             @include('vendor.includes.rightsidebar')

             @include('vendor.includes.footerlinks')


    </body>

</html>
