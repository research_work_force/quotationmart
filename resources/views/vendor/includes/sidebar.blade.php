<div class="vertical-menu">

    <div class="h-100">

        <div class="user-wid text-center py-4">
            <div class="user-img">
                <img src="{{asset('assets/images/users/avatar-2.jpg')}}" alt="" class="avatar-md mx-auto rounded-circle">
            </div>

            <div class="mt-3">

                <a href="#" class="text-dark font-weight-medium font-size-16">Vendor</a>
                <p class="text-body mt-1 mb-0 font-size-13">Vendor </p>

            </div>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
              <li>
                  <a href="{{ route('vendor.profile') }}"  class="waves-effect">
                      <i class="mdi mdi-account-star-outline"></i>
                      <span>Profile</span>
                  </a>

              </li>
              <li>
                  <a href="{{ route('vendor.order') }}"  class=" waves-effect">
                      <i class="mdi mdi-basket-fill"></i>
                      <span>Order</span>
                  </a>

                  <!-- <ul class="sub-menu" aria-expanded="false">

                      <li><a href="/customerprofilelist">Customer Profile List</a></li>
                      <li><a href="/profileupdateapproval">Profile Update Approval</a></li>
                      <li><a href="javascript: void(0);">Payment Details Approval</a></li>


                  </ul> -->

              </li>
              <li>
                  <a href="{{ route('vendor.enquiry') }}"  class=" waves-effect">
                      <i class="mdi mdi-layers-search"></i>
                      <span>Enquiries </span>
                  </a>



              </li>
              <li>
                  <a href="#"  class="waves-effect">
                      <i class="mdi mdi-email-receive-outline"></i>
                      <span>Inbox </span>
                  </a>



              </li>
              <li>
                  <a href="{{ route('vendor.productsearch') }}"  class=" waves-effect">
                    <i class="mdi mdi-movie-search"></i>
                      <span>Product Search </span>
                  </a>



              </li>

              <li>
                  <a href="{{ route('vendor.transaction') }}"  class=" waves-effect">
                      <i class="mdi mdi-chart-multiple"></i>
                      <span>Transaction and reports </span>
                  </a>



              </li>

              <li>
                  <a href="{{ route('vendor.payment') }}"  class="waves-effect">
                        <i class="mdi mdi-cash-usd-outline"></i>
                      <span>Payment</span>
                  </a>

              </li>
              <li>
                  <a href="{{ route('vendor.subscription') }}"  class="waves-effect">
                        <i class="mdi mdi-tag-text"></i>
                      <span>Subscription</span>
                  </a>

              </li>


              <li>
                  <a href="{{ route('customer.featuredvendors') }}" class="waves-effect">
                      <i class="mdi mdi-shield-star"></i>
                      <span>Pro Features </span>
                  </a>

              </li>










            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
