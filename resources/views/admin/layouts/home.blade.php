<!doctype html>
<html lang="en">

<head>
    <title> Admin Dashboard | Technomart</title>

    @include('admin.includes.headerlinks')

</head>

<body data-layout="detached" data-topbar="colored">

    <div class="container-fluid">
        <!-- Begin page -->
        <div id="layout-wrapper">

             @include('admin.includes.navbarheader')
             @include('admin.includes.sidebar')

             @yield('content')

             @include('admin.includes.footer')
         </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

    </div>
    <!-- end container-fluid -->

             @include('admin.includes.rightsidebar')

             @include('admin.includes.footerlinks')


    </body>

</html>
