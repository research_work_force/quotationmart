<div class="vertical-menu"> 

    <div class="h-100"> 

        <div class="user-wid text-center py-4">
            <div class="user-img">
                <img src="{{asset('assets/images/users/avatar-2.jpg')}}" alt="" class="avatar-md mx-auto rounded-circle">
            </div>

            <div class="mt-3">

                <a href="#" class="text-dark font-weight-medium font-size-16">Administrator</a>
                <p class="text-body mt-1 mb-0 font-size-13">System Admin</p>

            </div>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{route('admin.dashboard')}}" class="waves-effect">
                        <i class="mdi mdi-speedometer"></i>
                        <span>Dashboard</span>
                    </a>

                </li>
                <li>
                    <a  class="has-arrow waves-effect">
                        <i class="mdi mdi-account-multiple-check"></i>
                        <span>Customer </span>
                    </a>

                    <ul class="sub-menu" aria-expanded="false">

                        <li><a href="/customerprofilelist">Customer Profile List</a></li>
                        <li><a href="/profileupdateapproval">Profile Update Approval</a></li>
                        <li><a href="javascript: void(0);">Payment Details Approval</a></li>


                    </ul>

                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="mdi mdi-bank"></i>
                        <span>Vendor </span>
                    </a>

                    <ul class="sub-menu" aria-expanded="false">

                        <li><a href="javascript: void(0);">Vendor Profile List </a></li>
                        <li><a href="javascript: void(0);">Vendor Approvals</a></li>
                        <li><a href="javascript: void(0);">Enterprise & Premium Vendor Activity </a></li>


                    </ul>

                </li>
                <li>
                    <a href="javascript: void(0);" class=" has-arrow waves-effect">
                        <i class="mdi mdi-email-receive-outline"></i>
                        <span>Products </span>
                    </a>

                    <ul class="sub-menu" aria-expanded="false">

                      <li><a href="javascript: void(0);"><b>Vendor</b></a></li>

                        <li><a href="javascript: void(0);">Product List  </a></li>
                        <li><a href="javascript: void(0);">Product Entry Approval </a></li>
                        <li><a href="javascript: void(0);">Product Update Approval </a></li>
                        <li><a href="javascript: void(0);">Product Delete approval  </a></li>

                      <li><a href="javascript: void(0);"><b>Admin</b></a></li>

                      <li><a href="javascript: void(0);">Product Management  </a></li>



                    </ul>

                </li>

                <li>
                    <a href="javascript: void(0);" class=" has-arrow waves-effect">
                        <i class="mdi mdi-email-receive-outline"></i>
                        <span>Operation </span>
                    </a>

                    <ul class="sub-menu" aria-expanded="false">

                      <li><a href="javascript: void(0);"><b>Enquiry</b></a></li>

                        <li><a href="javascript: void(0);"> Vendor Enquiry List  </a></li>
                        <li><a href="javascript: void(0);"> Customer Enquiry List </a></li>
                        <li><a href="javascript: void(0);"> Enquiry Management</a></li>
                        <li><a href="javascript: void(0);"> Enquiry Cancellation  </a></li>
                        <li><a href="javascript: void(0);"> Enquiry Update Approval  </a></li>

                      <li><a href="javascript: void(0);"><b>Quotation </b></a></li>

                      <li><a href="{{route('admin.vendor.quatationcheckforword')}}">Vendor Quotation </a></li>
                      <li><a href="{{route('admin.vendor.askforrevisequatation')}}">Ask for Revise Quotation </a></li>
                      <li><a href="{{route('admin.vendorquotations')}}">Quotation Management </a></li>


                      <li><a href="javascript: void(0);"><b>Purchase Order  </b></a></li>

                      <li><a href="{{route('admin.customer.polist')}}"> Customer PO List  </a></li>
                      <li><a href="{{route('admin.pomanage')}}">PO Management </a></li>
                      <li><a href="{{route('admin.vendor.poupdateapproval')}}">PO Update Approval </a></li>
                      <li><a href="{{route('admin.livepo')}}">Live Orders Management </a></li>
                      <li><a href="javascript: void(0);">Live Orders Activities </a></li>


                      <li><a href="javascript: void(0);"><b>Work order  </b></a></li>

                      <li><a href="{{route('admin.wolist')}}"> Customer WO List  </a></li>
                      <li><a href="{{route('admin.wolistvendor')}}">WO approval </a></li>
                      <li><a href="{{route('admin.woliveprocess')}}">Live Orders activites </a></li>
                      
                      
                      
                      <li><a href="{{route('admin.woissue')}}">WO  Activities </a></li>




                    </ul>

                </li>

                <li>
                    <a href="javascript: void(0);" >
                        <i class="mdi mdi-note-text-outline"></i>
                        <span>Quotation </span>
                    </a>

                    

                </li>

                

                <li>
                    <a href="javascript: void(0);" class="waves-effect ">
                        <i class="mdi mdi-tanker-truck"></i>
                        <span>Shipping </span>
                    </a>

                </li>
                <li>
                    <a href="javascript: void(0);" class="waves-effect has-arrow">
                        <i class="mdi mdi-cash-usd-outline"></i>
                        <span>Payments </span>
                    </a>

                    <ul class="sub-menu" aria-expanded="false">

                        <li><a href="javascript: void(0);"><b>Customers </b></a></li>
                        <li><a href="{{route('customer.bankdetails')}}">Bank Details List  </a></li>
                        <li><a href="javascript: void(0);"> Bank Details Update  </a></li>
                        <li><a href="{{route('customer.livepopayhistory')}}"> Live PO payment history  </a></li>
                        <li><a href="{{route('customer.paymenthistory')}}">Payment history </a></li>
                        <li><a href="{{route('customer.subpayactivity')}}">Subscription wise Payments </a></li>
                         <li><a href="{{route('customer.payissue')}}">Payment related issue</a></li>

                        


                        <li><a href="javascript: void(0);"><b>Vendors </b></a></li>
                        <li><a href="{{route('vendor.bankdetails')}}"> Bank Details List  </a></li>
                        <li><a href="javascript: void(0);"> Bank Details Update  </a></li>
                        <li><a href="{{route('vendor.paymentsactivity')}}"> Payment activity </a></li>
                        <li><a href="{{route('vendor.shipmentpaymentsactivity')}}">Shipment Payment activity </a></li>
                        <li><a href="{{route('vendor.vendorpaymentgatewayactivity')}}">Payment gateway activity </a></li>


                    </ul>



                </li>


                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="mdi mdi-printer-check"></i>
                        <span>Invoice </span>
                    </a>

                </li>

                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="mdi mdi-chart-multiple"></i>
                        <span>Transaction </span>
                    </a>

                </li>
                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="mdi mdi-movie-search"></i>
                        <span>Monitoring</span>
                    </a>

                </li>
                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="mdi mdi-beaker-check"></i>
                        <span>Forms </span>
                    </a>

                </li>
                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="mdi mdi-folder-settings-variant-outline"></i>
                        <span>Miscellaneous</span>
                    </a>

                </li>

                <li>
                    <a href="javascript: void(0);" class="waves-effect has-arrow">
                        <i class="bx bx-support"></i>
                        <span>Support </span>
                    </a>

                    <ul class="sub-menu" aria-expanded="false">

                        <li><a href="{{route('admin.enquiryrelatedissue')}}">Enquiry Related Issue</a></li>

                        <li><a href="{{route('admin.quotationrelatedissue')}}">Quotation Related Issue</a></li>

                        <li><a href="{{route('admin.porelatedissue')}}">PO Related Issue</a></li>
                        <li><a href="{{route('admin.worelatedissue')}}">WO Related Issue</a></li>

                        <li><a href="{{route('admin.shipmentrelatedissue')}}">Shipment Related Issue</a></li>

                        <li><a href="{{route('admin.transactionrelatedissue')}}">Transaction Related Issue</a></li>


                    </ul>



                </li>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
