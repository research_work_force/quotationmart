@extends('admin.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">PO related issue</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                            <li class="breadcrumb-item active">PO related issue</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">List of issues</h1>
                        <!-- <p class="card-title-desc">
                            “It is not the employer who pays the wages. Employers only handle the money. It is the customer who pays the wages.” – Henry Ford
                        </p> -->

                        <div class="row">
                        			
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="PO No">
                                      </div> 
                                      
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Company name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="date" placeholder="PO Date">
                                      </div>
                                      <!-- <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Issue no">
                                      </div> -->
                                      <!-- <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Company name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="date" placeholder="Company name">
                                      </div> -->
                                      
                                      <div class="col-md-3 mb-4">
                                        <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                          <i class="bx bx-loader bx-search font-size-16 align-middle mr-2"></i> Find issue
                                      </button>
                                      </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
<!-- 
        <div class="row">
          <div class="col-4">
              <h4>Total PO value: 5000/month<h4>
          </div>
          <div class="col-5">
            <h4>  Total PO value: 2600000/year <h4>
          </div>

            <div class="col-3">
              <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                Download PO History
            </button>
            </div>
        </div> -->

        <!-- Second Card -->

                <div class="row">
                    <div class="col-lg-12">
                    	
                                <div class="table-responsive">
                                    <table class="table table-thinkagainlab mb-0 ">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th nowrap="nowrap">PO no</th>
                                                <th nowrap="nowrap">PO date </th>
                                                <th nowrap="nowrap">Company name </th>
                                                <th nowrap="nowrap">Vendor name</th>
                                                <th nowrap="nowrap">PO Short Details</th>
                                                <th nowrap="nowrap">Admin Listed Product</th>
                                                <th nowrap="nowrap">Vendor Listed Product</th>
                                                <th nowrap="nowrap">Quotation NO</th>
                                                <th nowrap="nowrap">Quotation Short Details </th>
                                                <th nowrap="nowrap">PO Status </th>
                                                <th nowrap="nowrap">PO Amount</th>
                                                <th nowrap="nowrap">Delivery Period </th>
                                                   <th nowrap="nowrap">Payment Terms</th>
                                                <th nowrap="nowrap">Shipment Req</th>
                                                <th nowrap="nowrap">PI No </th>
                                                <th nowrap="nowrap">Shipment Process </th>
                                                <th nowrap="nowrap">Material Receive</th>
                                                <th nowrap="nowrap">Payment </th>
                                                <th nowrap="nowrap">Issue Type</th>
                                                <th nowrap="nowrap">Issue No </th>
                                                <th nowrap="nowrap">Issue Status </th>
                                                <th nowrap="nowrap">Issue Short Details</th>
                                                <th nowrap="nowrap">Issue Raise Date </th>
                                                <th nowrap="nowrap">Issue Solve Date </th>
                                                
                                              
                                                                                             
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        	
                                            <tr>    

                                                <th scope="row">1</th>
                                                <td>XYZ name</td>
                                                <td>Hello</td>
                                                <td>Here it is </td>
                                                <td>Compleated</td>
                                                <td>2500</td>
                                                <td>ABC pvt ltd.</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>XYZ name</td>
                                                <td>Hello</td>
                                                <td>Here it is </td>
                                                <td>Compleated</td>
                                                <td>2500</td>
                                                <td>ABC pvt ltd.</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                 
                                               
                                                
                                                

                                                <td>

                                               <div class="btn-group mt-2 mr-1 dropleft">     
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light dropdown-toggle" data-toggle="dropdown" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">View & download PO</a>
                                            <a class="dropdown-item" href="#">View & download issue</a>
                                            <a class="dropdown-item" href="#">View & Download Quotation</a>
                                            <a class="dropdown-item" href="#">View & Download Activities</a>
                                            <a class="dropdown-item" href="#">Issue check and forward</a>
                                            <a class="dropdown-item" href="#">Issue Solve</a>
                                         
                                          
                                            
                                                 </div>
                                             </div>

                                                </td>

                                            </tr>

                                               <tr>    

                                                <th scope="row">1</th>
                                                <td>XYZ name</td>
                                                <td>Hello</td>
                                                <td>Here it is </td>
                                                <td>Compleated</td>
                                                <td>2500</td>
                                                <td>ABC pvt ltd.</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>XYZ name</td>
                                                <td>Hello</td>
                                                <td>Here it is </td>
                                                <td>Compleated</td>
                                                <td>2500</td>
                                                <td>ABC pvt ltd.</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                 
                                               
                                                
                                                

                                                <td>

                                               <div class="btn-group mt-2 mr-1 dropleft">     
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light dropdown-toggle" data-toggle="dropdown" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">View & download PO</a>
                                            <a class="dropdown-item" href="#">View & download issue</a>
                                            <a class="dropdown-item" href="#">View & Download Quotation</a>
                                            <a class="dropdown-item" href="#">View & Download Activities</a>
                                            <a class="dropdown-item" href="#">Issue check and forward</a>
                                            <a class="dropdown-item" href="#">Issue Solve</a>
                                         
                                          
                                            
                                                 </div>
                                             </div>

                                                </td>

                                            </tr>


                                              



                                        </tbody>
                                    </table>
                                </div>


                    </div>
                </div>
        <!-- Second row end -->

    </div>
    <!-- End Page-content -->

@endsection
