@extends('admin.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Customer Enquiry List</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Operation</a></li>
                            <li class="breadcrumb-item active">Customer Enquiry List</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">List of Enquiries from Customer</h1>
                        <!-- <p class="card-title-desc">
                            “It is not the employer who pays the wages. Employers only handle the money. It is the customer who pays the wages.” – Henry Ford
                        </p> -->

                        <div class="row">
                                      <div class="col-md-2 mb-4">
                                          <input class="form-control" type="text" placeholder="Enquiry Id">
                                      </div>
                                      <div class="col-md-2 mb-4">
                                          <input class="form-control" type="date" placeholder="Enquiry Date">
                                      </div>
                                      <div class="col-md-2 mb-4">
                                          <input class="form-control" type="text" placeholder="Customer Name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Admin product name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                        <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                          <i class="bx bx-loader bx-search font-size-16 align-middle mr-2"></i> Find Enquiry
                                      </button>
                                      </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

        <!-- Second Card -->

                <div class="row">
                    <div class="col-lg-12">

                                <div class="table-responsive">
                                    <table class="table table-thinkagainlab mb-0 ">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Enquiry No</th>
                                                <th>Enquiry Date</th>
                                                <th>Customer Name</th>
                                                <th>Company Name</th>
                                                <th>Enquiry Short Description</th>
                                                <th>Admin Listed Products</th>


                                                <th>Action  </th>






                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Elas545</td>
                                                <td>02/06/2019</td>
                                                <td>Alksja Hjksdlksj</td>
                                                <td>lsadasdjsad aoshdooasd</td>
                                                <td>slkd sjdlsj</td>
                                                <td>slkfjjsdf kfjsdlfj</td>


                                                <td>
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                </td>

                                            </tr>

                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Elas545</td>
                                                <td>02/06/2019</td>
                                                <td>Alksja Hjksdlksj</td>
                                                <td>lsadasdjsad aoshdooasd</td>
                                                <td>slkd sjdlsj</td>
                                                <td>slkfjjsdf kfjsdlfj</td>


                                                <td>
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                </td>

                                            </tr>

                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Elas545</td>
                                                <td>02/06/2019</td>
                                                <td>Alksja Hjksdlksj</td>
                                                <td>lsadasdjsad aoshdooasd</td>
                                                <td>slkd sjdlsj</td>
                                                <td>slkfjjsdf kfjsdlfj</td>


                                                <td>
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                </td>

                                            </tr>






                                        </tbody>
                                    </table>
                                </div>


                    </div>
                </div>
        <!-- Second row end -->

    </div>
    <!-- End Page-content -->








@endsection
