@extends('admin.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Vendor Product Delete Approval</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                            <li class="breadcrumb-item active">Vendor Product Delete Approval</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
                              <div class="col-md-12 col-xl-4">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row align-items-center">
                                              <div class="col-8">
                                                  <p class="mb-2">Completed Approvals</p>
                                                  <h4 class="mb-0">3,524</h4>
                                              </div>
                                              <div class="col-4">
                                                  <div class="text-right">
                                                      <div>
                                                          2.06 % <i class="mdi mdi-arrow-up text-success ml-1"></i>
                                                      </div>
                                                      <div class="progress progress-sm mt-3">
                                                          <div class="progress-bar bg-success" role="progressbar" style="width: 62%" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-md-12 col-xl-4">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row align-items-center">
                                              <div class="col-8">
                                                  <p class="mb-2">Pending Approvals</p>
                                                  <h4 class="mb-0">5,362</h4>
                                              </div>
                                              <div class="col-4">
                                                  <div class="text-right">
                                                      <div>
                                                          3.12 % <i class="mdi mdi-arrow-up text-success ml-1"></i>
                                                      </div>
                                                      <div class="progress progress-sm mt-3">
                                                          <div class="progress-bar bg-warning" role="progressbar" style="width: 78%" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-md-12 col-xl-4">
                                  <div class="card">
                                      <div class="card-body">
                                          <div class="row align-items-center">
                                              <div class="col-8">
                                                  <p class="mb-2">Rejected Approvals</p>
                                                  <h4 class="mb-0">6,245</h4>
                                              </div>
                                              <div class="col-4">
                                                  <div class="text-right">
                                                      <div>
                                                          2.12 % <i class="mdi mdi-arrow-up text-success ml-1"></i>
                                                      </div>
                                                      <div class="progress progress-sm mt-3">
                                                          <div class="progress-bar bg-danger" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">List of Product Delete Requests</h1>
                        <!-- <p class="card-title-desc">
                          “If you don't appreciate your Vendors, someone else will.”
― Jason Langella
                        </p> -->



                        <div class="row">
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Vendor ID">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Vendor Name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Vendor Email">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                        <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                          <i class="bx bx-loader bx-search font-size-16 align-middle mr-2"></i> Find a Product
                                      </button>
                                      </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>



        <div class="row">
          <div class="col-md-12 col-xl-4">
              <div class="card">
                  <div class="card-body">
                      <div class="row align-items-center">
                          <div class="col-8">
                              <p class="mb-2 card-heading-id" style="" align="center"><b>Vendor ID:</b> asoaj56464</p>
                              <p class="mb-2"><b>Product Name:</b> Profile Name</p>
                              <p class="mb-2"><b>Category:</b> Alkslkja Hasmas</p>
                              <!-- <p class="mb-2"><b>Designation:</b> Mr. Alkslkja Hasmas</p> -->

                              <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:60%;">
                                View Details
                            </button>
                              <!-- <h4 class="mb-0">5,362</h4> -->
                          </div>
                          <div class="col-4">
                              <div class="text-right">
                                  <div style="margin-bottom: 6%;">
                                    <button type="button" class="btn btn-success  waves-effect waves-light" style="width: 50%;" >
                                      <i class="fas fa-check"></i>
                                  </button>
                                  </div>
                                  <div >
                                    <button type="button" class="btn btn-danger  waves-effect waves-light" style="width: 50%;">
                                      <i class="fas fa-times"></i>
                                  </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="col-md-12 col-xl-4">
              <div class="card">
                  <div class="card-body">
                      <div class="row align-items-center">
                          <div class="col-8">
                              <p class="mb-2 card-heading-id" style="" align="center"><b>Vendor ID:</b> asoaj56464</p>
                              <p class="mb-2"><b>Product Name:</b> Profile Name</p>
                              <p class="mb-2"><b>Category:</b> Alkslkja Hasmas</p>
                              <!-- <p class="mb-2"><b>Designation:</b> Mr. Alkslkja Hasmas</p> -->

                              <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:60%;">
                                View Details
                            </button>
                              <!-- <h4 class="mb-0">5,362</h4> -->
                          </div>
                          <div class="col-4">
                              <div class="text-right">
                                  <div style="margin-bottom: 6%;">
                                    <button type="button" class="btn btn-success  waves-effect waves-light" style="width: 50%;" >
                                      <i class="fas fa-check"></i>
                                  </button>
                                  </div>
                                  <div >
                                    <button type="button" class="btn btn-danger  waves-effect waves-light" style="width: 50%;">
                                      <i class="fas fa-times"></i>
                                  </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>  <div class="col-md-12 col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <p class="mb-2 card-heading-id" style="" align="center"><b>Vendor ID:</b> asoaj56464</p>
                                <p class="mb-2"><b>Product Name:</b> Profile Name</p>
                                <p class="mb-2"><b>Category:</b> Alkslkja Hasmas</p>
                                <!-- <p class="mb-2"><b>Designation:</b> Mr. Alkslkja Hasmas</p> -->

                                <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:60%;">
                                  View Details
                              </button>
                                <!-- <h4 class="mb-0">5,362</h4> -->
                            </div>
                            <div class="col-4">
                                <div class="text-right">
                                    <div style="margin-bottom: 6%;">
                                      <button type="button" class="btn btn-success  waves-effect waves-light" style="width: 50%;" >
                                        <i class="fas fa-check"></i>
                                    </button>
                                    </div>
                                    <div >
                                      <button type="button" class="btn btn-danger  waves-effect waves-light" style="width: 50%;">
                                        <i class="fas fa-times"></i>
                                    </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  <div class="col-md-12 col-xl-4">
                  <div class="card">
                      <div class="card-body">
                          <div class="row align-items-center">
                              <div class="col-8">
                                  <p class="mb-2 card-heading-id" style="" align="center"><b>Vendor ID:</b> asoaj56464</p>
                                  <p class="mb-2"><b>Product Name:</b> Profile Name</p>
                                  <p class="mb-2"><b>Category:</b> Alkslkja Hasmas</p>
                                  <!-- <p class="mb-2"><b>Designation:</b> Mr. Alkslkja Hasmas</p> -->

                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:60%;">
                                    View Details
                                </button>
                                  <!-- <h4 class="mb-0">5,362</h4> -->
                              </div>
                              <div class="col-4">
                                  <div class="text-right">
                                      <div style="margin-bottom: 6%;">
                                        <button type="button" class="btn btn-success  waves-effect waves-light" style="width: 50%;" >
                                          <i class="fas fa-check"></i>
                                      </button>
                                      </div>
                                      <div >
                                        <button type="button" class="btn btn-danger  waves-effect waves-light" style="width: 50%;">
                                          <i class="fas fa-times"></i>
                                      </button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>  <div class="col-md-12 col-xl-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <p class="mb-2 card-heading-id" style="" align="center"><b>Vendor ID:</b> asoaj56464</p>
                                    <p class="mb-2"><b>Product Name:</b> Profile Name</p>
                                    <p class="mb-2"><b>Category:</b> Alkslkja Hasmas</p>
                                    <!-- <p class="mb-2"><b>Designation:</b> Mr. Alkslkja Hasmas</p> -->

                                    <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:60%;">
                                      View Details
                                  </button>
                                    <!-- <h4 class="mb-0">5,362</h4> -->
                                </div>
                                <div class="col-4">
                                    <div class="text-right">
                                        <div style="margin-bottom: 6%;">
                                          <button type="button" class="btn btn-success  waves-effect waves-light" style="width: 50%;" >
                                            <i class="fas fa-check"></i>
                                        </button>
                                        </div>
                                        <div >
                                          <button type="button" class="btn btn-danger  waves-effect waves-light" style="width: 50%;">
                                            <i class="fas fa-times"></i>
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  <div class="col-md-12 col-xl-4">
                      <div class="card">
                          <div class="card-body">
                              <div class="row align-items-center">
                                  <div class="col-8">
                                      <p class="mb-2 card-heading-id" style="" align="center"><b>Vendor ID:</b> asoaj56464</p>
                                      <p class="mb-2"><b>Product Name:</b> Profile Name</p>
                                      <p class="mb-2"><b>Category:</b> Alkslkja Hasmas</p>
                                      <!-- <p class="mb-2"><b>Designation:</b> Mr. Alkslkja Hasmas</p> -->

                                      <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:60%;">
                                        View Details
                                    </button>
                                      <!-- <h4 class="mb-0">5,362</h4> -->
                                  </div>
                                  <div class="col-4">
                                      <div class="text-right">
                                          <div style="margin-bottom: 6%;">
                                            <button type="button" class="btn btn-success  waves-effect waves-light" style="width: 50%;" >
                                              <i class="fas fa-check"></i>
                                          </button>
                                          </div>
                                          <div >
                                            <button type="button" class="btn btn-danger  waves-effect waves-light" style="width: 50%;">
                                              <i class="fas fa-times"></i>
                                          </button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>








        </div>
        <!-- end row -->



    </div>
    <!-- End Page-content -->








@endsection
