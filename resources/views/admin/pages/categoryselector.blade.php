<!-- erase later: delete the file  -->
<div>
    @php
    $categories = App\Http\Controllers\CategoryController::fetchCategories();
    @endphp
    <select name="categoryselector" id="categselec">
        <option value="">--Select Category--</option>
        @foreach($categories as $category)
        <option value="{{ $category->id }}">
            {{ $category->categoryname }}
        </option>
        @endforeach
    </select>
    <select name="subcategoryselector" id="subcategselec">
        <option>--Select Sub-Category--</option>
    </select>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $('select[name="categoryselector"]').on('change', function() {
            $categoryid = $(this).val();
            if ($categoryid) {
                $.ajax({
                    url: '/adminproductlist/getsubcategories/' + $categoryid,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        console.log(data);

                        $('select[name="subcategoryselector"]').empty();

                        $.each(data, function(key, value) {
                            $('select[name="subcategoryselector"]').append('<option value="' + key + '">' + value + '</option>');

                        });
                    },
                    error: function(e) {
                        console.log(e);

                    },
                    complete: function() {
                        $('#loader').css("visibility", "hidden");
                    }

                });
            } else {
                $('select[name="subcategoryselector"]').empty();
            }
        })
    })
</script>