@extends('admin.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Vendor quatation</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                            <li class="breadcrumb-item active">Vendor quation check and forword</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">List of quotation</h1>
                        <!-- <p class="card-title-desc">
                            “It is not the employer who pays the wages. Employers only handle the money. It is the customer who pays the wages.” – Henry Ford
                        </p> -->

                        <div class="row">
                        			<div class="col-md-3 mb-4">
                                          <input class="form-control" type="date" placeholder="Quatation Date">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Quatation no">
                                      </div>
                                      
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Vendor Name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Company Name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Enquiry No">
                                      </div>
                                      
                                      <div class="col-md-3 mb-4">
                                        <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                          <i class="bx bx-loader bx-search font-size-16 align-middle mr-2"></i> Find quotation
                                      </button>
                                      </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
         
          

            <!-- <div class="col-3">
              <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                Download Enquiry History
            </button>
            </div> -->
        </div>

        <!-- Second Card -->

                <div class="row">
                    <div class="col-lg-12">

                                <div class="table-responsive">
                                    <table class="table table-thinkagainlab mb-0 ">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Enquiry No</th>
                                                <th>Quatation no </th>
                                                <th nowrap="nowrap">Enquired customer name </th>
                                                <th nowrap="nowrap">Enquired company name</th>
                                                <th nowrap="nowrap">Enquiry short description</th>
                                                <th nowrap="nowrap">Vendor name  </th>
                                                <th nowrap="nowrap">Vendor listed related product</th>
                                                <th nowrap="nowrap">Admin listed related product</th>
                                                <th nowrap="nowrap">Quatation check status </th>
                                                <th nowrap="nowrap">Quatation forword</th>
                                                <th nowrap="nowrap">Technical discussion & negotiation status</th>
                                                
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>    

                                                <th scope="row">1</th>
                                                <td>XYZ/ENQR/2020/16</td>
                                                <td>XYZ/QUO/2025/15</td>
                                                <td>Mr. Hello</td>
                                                <td>ABX pvt ltd.</td>
                                                <td>Here it is</td>
                                                <td>Vendor</td>
                                                <td>Product</td>
                                                <td>Product</td>
                                                <td>Resolved</td>
                                                <td>Issue no</td>
                                                <td>Processing</td>
                                            

                                                <td>

                                               <div class="btn-group mt-2 mr-1 dropleft">     
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light dropdown-toggle" data-toggle="dropdown" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Ask for revision</a>
                                            <a class="dropdown-item" href="#">View and  download</a>
                                            <a class="dropdown-item" href="#">Check & update</a>
                                            <a class="dropdown-item" href="#">Technical Discussion & Negotiation Status</a>
                                                 </div>
                                             </div>

                                                </td>

                                            </tr>

                                            <tr>    

                                                <th scope="row">1</th>
                                                <td>XYZ/ENQR/2020/16</td>
                                                <td>XYZ/QUO/2025/15</td>
                                                <td>Mr. Hello</td>
                                                <td>ABX pvt ltd.</td>
                                                <td>Here it is</td>
                                                <td>Vendor</td>
                                                <td>Product</td>
                                                <td>Product</td>
                                                <td>Resolved</td>
                                                <td>Issue no</td>
                                                <td>Processing</td>
                                            

                                                 <td>

                                               <div class="btn-group mt-2 mr-1 dropleft">     
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light dropdown-toggle" data-toggle="dropdown" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Ask for revision</a>
                                            <a class="dropdown-item" href="#">View and  download</a>
                                            <a class="dropdown-item" href="#">Check & update</a>
                                            <a class="dropdown-item" href="#">Technical Discussion & Negotiation Status</a>
                                                 </div>
                                             </div>

                                                </td>
                                            </tr>

                                            <tr>    

                                                <th scope="row">1</th>
                                                <td>XYZ/ENQR/2020/16</td>
                                                <td>XYZ/QUO/2025/15</td>
                                                <td>Mr. Hello</td>
                                                <td>ABX pvt ltd.</td>
                                                <td>Here it is</td>
                                                <td>Vendor</td>
                                                <td>Product</td>
                                                <td>Product</td>
                                                <td>Resolved</td>
                                                <td>Issue no</td>
                                                <td>Processing</td>
                                            

                                                 <td>

                                               <div class="btn-group mt-2 mr-1 dropleft">     
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light dropdown-toggle" data-toggle="dropdown" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Ask for revision</a>
                                            <a class="dropdown-item" href="#">View and  download</a>
                                            <a class="dropdown-item" href="#">Check & update</a>
                                            <a class="dropdown-item" href="#">Technical Discussion & Negotiation Status</a>
                                                 </div>
                                             </div>

                                                </td>
                                            </tr>


                                        </tbody>
                                    </table>
                                </div>


                    </div>
                </div>
        <!-- Second row end -->

    </div>
    <!-- End Page-content -->

@endsection
