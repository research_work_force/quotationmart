@extends('admin.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Shipment related issue</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                            <li class="breadcrumb-item active">Shipment related issues</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">List of issues</h1>
                        <!-- <p class="card-title-desc">
                            “It is not the employer who pays the wages. Employers only handle the money. It is the customer who pays the wages.” – Henry Ford
                        </p> -->

                        <div class="row">
                        			
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Track id">
                                      </div> 
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Company name">
                                      </div>

                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Issue No">
                                      </div>  
                                      
                                     
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="date" placeholder="PO Date">
                                      </div>
                                      <!-- <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Issue no">
                                      </div> -->
                                      <!-- <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Company name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="date" placeholder="Company name">
                                      </div> -->
                                      
                                      <div class="col-md-3 mb-4">
                                        <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                          <i class="bx bx-loader bx-search font-size-16 align-middle mr-2"></i> Find issue
                                      </button>
                                      </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
<!-- 
        <div class="row">
          <div class="col-4">
              <h4>Total PO value: 5000/month<h4>
          </div>
          <div class="col-5">
            <h4>  Total PO value: 2600000/year <h4>
          </div>

            <div class="col-3">
              <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                Download PO History
            </button>
            </div>
        </div> -->

        <!-- Second Card -->

                <div class="row">
                    <div class="col-lg-12">
                    	
                                <div class="table-responsive">
                                    <table class="table table-thinkagainlab mb-0 ">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th nowrap="nowrap">Shipment Req. No</th>
                                                <th nowrap="nowrap">PO No </th>
                                                <th nowrap="nowrap">PO Date</th>
                                                <th nowrap="nowrap">PO Short Details</th>
                                                <th nowrap="nowrap">Track ID</th>
                                                <th nowrap="nowrap">Shipment Details</th>
                                                <th nowrap="nowrap">Company Name</th>
                                                <th nowrap="nowrap">Vendor Name</th>
                                                <th nowrap="nowrap">Pick Up Pin code </th>
                                                <th nowrap="nowrap">Drop Pin Code </th>
                                                <th nowrap="nowrap">Shipment Short Details</th>
                                                <th nowrap="nowrap">Shipment Invoice No</th>
                                                   <th nowrap="nowrap">Shipment Amount</th>
                                                <th nowrap="nowrap">Shipment Status</th>
                                                <th nowrap="nowrap">Material Received Date</th>
                                                <th nowrap="nowrap">Material Receive Status </th>
                                                <th nowrap="nowrap">Courier Partner</th>
                                                <th nowrap="nowrap">Customer Invoice No</th>
                                                <th nowrap="nowrap">Proforma Invoice No</th>
                                                <th nowrap="nowrap">Invoice & Label Generate Status</th>
                                                <th nowrap="nowrap">Issue No</th>
                                                <th nowrap="nowrap">Issue Status</th>
                                                <th nowrap="nowrap">Issue Short Details </th>
                                                <th nowrap="nowrap">Issue Raise Date </th>
                                                <th nowrap="nowrap">Issue Solve Date </th>
                                                
                                              
                                                                                             
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        	
                                            <tr>    

                                                <th scope="row">1</th>
                                                <td>XYZ name</td>
                                                <td>Hello</td>
                                                <td>Here it is </td>
                                                <td>Compleated</td>
                                                <td>2500</td>
                                                <td>ABC pvt ltd.</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>XYZ name</td>
                                                <td>Hello</td>
                                                <td>Here it is </td>
                                                <td>Compleated</td>
                                                <td>2500</td>
                                                <td>ABC pvt ltd.</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                 
                                               
                                                
                                                

                                                <td>

                                               <div class="btn-group mt-2 mr-1 dropleft">     
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light dropdown-toggle" data-toggle="dropdown" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">View & Download Shipment Details</a>
                                            <a class="dropdown-item" href="#">View & download issue</a>
                                            <a class="dropdown-item" href="#">View & Download PO</a>
                                            <a class="dropdown-item" href="#">Track Shipment</a>
                                            
                                            <a class="dropdown-item" href="#">Check and forword</a>
                                            
                                            <a class="dropdown-item" href="#">Issue Solve</a>
                                         
                                          
                                            
                                                 </div>
                                             </div>

                                                </td>

                                            </tr>

                                               


                                              



                                        </tbody>
                                    </table>
                                </div>


                    </div>
                </div>
        <!-- Second row end -->

    </div>
    <!-- End Page-content -->

@endsection
