@extends('admin.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Admin Product Management</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Product</a></li>
                            <li class="breadcrumb-item active">Admin Product Management</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">Manage your Products</h1>
                        <!-- <p class="card-title-desc">
                            “It is not the employer who pays the wages. Employers only handle the money. It is the customer who pays the wages.” – Henry Ford
                        </p> -->

                        <div class="row">
                            <div class="col-md-3 mb-4">
                                <input class="form-control" type="text" placeholder="Product ID">
                            </div>
                            <div class="col-md-3 mb-4">
                                <input class="form-control" type="text" placeholder="Product Name">
                            </div>
                            <div class="col-md-3 mb-4">
                                <input class="form-control" type="text" placeholder="Product Catagory">
                            </div>
                            <div class="col-md-3 mb-4">
                                <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                    <i class="bx bx-loader bx-search font-size-16 align-middle mr-2"></i> Find a Product
                                </button>
                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-6">

                @if(Session::has('message'))
                <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
                @endif

            </div>
            <div class="col-2">
                <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;" data-toggle="modal" data-target=".catagoryadd-modal-center">
                    Create category
                </button>
                @include('admin.pages.modals.categoryaddmodal')
            </div>
            <div class="col-2">
                <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;" data-toggle="modal" data-target=".subcategoryadd-modal-center">
                    Create sub-category
                </button>
                @include('admin.pages.modals.subcategoryaddmodal')
            </div>
            <div class="col-2">
                <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;" data-toggle="modal" data-target=".productadd-modal-center">
                    Create Product
                </button>
            </div>
            @include('admin.pages.modals.productaddmodal')
        </div>

        <!-- Second Card -->

        <div class="row">
            <div class="col-lg-12">

                <div class="table-responsive">
                    <table class="table table-thinkagainlab mb-0 ">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Product ID</th>
                                <th>Vendor ID</th>
                                <th>Category</th>
                                <th>Sub Catagory</th>
                                <th>HSN Code</th>
                                <th>Product Type</th>

                                <th>Action </th>


                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell Table cellTable cell</td>

                                <td>
                                    <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light">
                                        <i class="fas fa-cogs"></i>
                                    </button>

                                </td>

                            </tr>


                            <tr>
                                <th scope="row">1</th>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell Table cellTable cell</td>

                                <td>
                                    <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light">
                                        <i class="fas fa-cogs"></i>
                                    </button>

                                </td>

                            </tr>

                            <tr>
                                <th scope="row">1</th>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell</td>
                                <td>Table cell Table cellTable cell</td>

                                <td>
                                    <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light">
                                        <i class="fas fa-cogs"></i>
                                    </button>

                                </td>

                            </tr>



                        </tbody>
                    </table>
                </div>


            </div>
        </div>
        <!-- Second row end -->

    </div>
    <!-- End Page-content -->








    @endsection