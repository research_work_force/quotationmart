@extends('admin.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Live PO payment history</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Admin</a></li>
                            <li class="breadcrumb-item active">Live PO payment history</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">List of payments</h1>
                        <!-- <p class="card-title-desc">
                            “It is not the employer who pays the wages. Employers only handle the money. It is the customer who pays the wages.” – Henry Ford
                        </p> -->

                        <div class="row">
                        			
                                      <!-- <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="WO No">
                                      </div>  -->
                                      
                                      <div class="col-md-2 mb-4">
                                          <input class="form-control" type="text" placeholder="company Name">
                                      </div>
                                      <div class="col-md-3 mb-2">
                                          <input class="form-control" type="date" placeholder="due date">
                                      </div>
                                      <div class="col-md-2 mb-3">
                                          <input class="form-control" type="text" placeholder="payment status">
                                      </div>
                                      <div class="col-md-2 mb-3">
                                          <input class="form-control" type="text" placeholder="PO No">
                                      </div>
                                      
                                      <div class="col-md-3 mb-4">
                                        <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                          <i class="bx bx-loader bx-search font-size-16 align-middle mr-2"></i> Find details
                                      </button>
                                      </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
<!-- 
        <div class="row">
          <div class="col-4">
              <h4>Total PO value: 5000/month<h4>
          </div>
          <div class="col-5">
            <h4>  Total PO value: 2600000/year <h4>
          </div>

            <div class="col-3">
              <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                Download PO History
            </button>
            </div>
        </div> -->

        <!-- Second Card -->

                <div class="row">
                    <div class="col-lg-12">
                    	
                                <div class="table-responsive">
                                    <table class="table table-thinkagainlab mb-0 ">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th nowrap="nowrap">PO no</th>
                                                <th nowrap="nowrap">PO date</th>
                                                <th nowrap="nowrap">Company name</th>
                                                <th nowrap="nowrap">Customer name</th>
                                                <th nowrap="nowrap">PO short details</th>
                                                <th nowrap="nowrap">Amount </th>
                                                <th nowrap="nowrap">Payment terms</th>
                                                <th nowrap="nowrap">Payment status</th>
                                                <th nowrap="nowrap">Payment due date</th>
                                                 <th nowrap="nowrap">Payment compleate (%)</th>
                                                <th nowrap="nowrap">Last payment date</th>
                                                <th nowrap="nowrap">Last payment details</th>
                                                
                                                <!-- <th nowrap="nowrap">Proposed duration</th>
                                                <th nowrap="nowrap">Gate Pass Status</th>
                                                <th nowrap="nowrap">MOM status</th>
                                                <th nowrap="nowrap">WO status</th>
                                                <th nowrap="nowrap">MOM issue date</th>
                                                <th nowrap="nowrap">Revisions</th> -->
                                                
                                                                                             
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>    

                                                <th scope="row">1</th>
                                                <td>XYZ name</td>
                                                <td>Hello</td>
                                                <td>Here it is </td>
                                                <td>Compleated</td>
                                                <td>2500</td>
                                                <td>ABC pvt ltd.</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                 <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                 
                                               
                                                
                                                

                                                <td>

                                               <div class="btn-group mt-2 mr-1 dropleft">     
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light dropdown-toggle" data-toggle="dropdown" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">View & download </a>
                                            <a class="dropdown-item" href="#">View & download PO</a>
                                            <a class="dropdown-item" href="#">Update </a>
                                            <a class="dropdown-item" href="#">Follow up</a>
                                         
                                           <!--  <a class="dropdown-item" href="#">View & download gate pass</a>
                                            <a class="dropdown-item" href="#">View & Download MOM</a>
                                            <a class="dropdown-item" href="#">Chat communication</a> -->
                                            
                                                 </div>
                                             </div>

                                                </td>

                                            </tr>

                                              <tr>    

                                                <th scope="row">1</th>
                                                <td>XYZ name</td>
                                                <td>Hello</td>
                                                <td>Here it is </td>
                                                <td>Compleated</td>
                                                <td>2500</td>
                                                <td>ABC pvt ltd.</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                 <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                 
                                               
                                                
                                                

                                                <td>

                                               <div class="btn-group mt-2 mr-1 dropleft">     
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light dropdown-toggle" data-toggle="dropdown" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">View & download </a>
                                            <a class="dropdown-item" href="#">View & download PO</a>
                                            <a class="dropdown-item" href="#">Update </a>
                                            <a class="dropdown-item" href="#">Follow up</a>
                                         
                                           <!--  <a class="dropdown-item" href="#">View & download gate pass</a>
                                            <a class="dropdown-item" href="#">View & Download MOM</a>
                                            <a class="dropdown-item" href="#">Chat communication</a> -->
                                            
                                                 </div>
                                             </div>

                                                </td>

                                            </tr>



                                              <tr>    

                                                <th scope="row">1</th>
                                                <td>XYZ name</td>
                                                <td>Hello</td>
                                                <td>Here it is </td>
                                                <td>Compleated</td>
                                                <td>2500</td>
                                                <td>ABC pvt ltd.</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                 <td>Table cell</td>
                                                <td>Table cell</td> 
                                                <td>Table cell</td>
                                                 
                                               
                                                
                                                

                                                <td>

                                               <div class="btn-group mt-2 mr-1 dropleft">     
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light dropdown-toggle" data-toggle="dropdown" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">View & download </a>
                                            <a class="dropdown-item" href="#">View & download PO</a>
                                            <a class="dropdown-item" href="#">Update </a>
                                            <a class="dropdown-item" href="#">Follow up</a>
                                         
                                           <!--  <a class="dropdown-item" href="#">View & download gate pass</a>
                                            <a class="dropdown-item" href="#">View & Download MOM</a>
                                            <a class="dropdown-item" href="#">Chat communication</a> -->
                                            
                                                 </div>
                                             </div>

                                                </td>

                                            </tr>



                                        </tbody>
                                    </table>
                                </div>


                    </div>
                </div>
        <!-- Second row end -->

    </div>
    <!-- End Page-content -->

@endsection
