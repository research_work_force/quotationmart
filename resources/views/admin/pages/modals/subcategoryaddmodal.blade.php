<div class="modal fade subcategoryadd-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Create Sub-category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="subcategory" method='POST' action="{{ route('admin.pages.adminproductlist.subcategorycreation') }}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-12">
                            @php
                            $categories = App\Http\Controllers\CategoryController::fetchCategories();
                            @endphp
                            <select class="form-control" name="categoryid" required>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}">
                                    {{ $category->categoryname }}
                                </option>
                                @endforeach
                            </select>

                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12">
                            <input type="text" placeholder="Sub-category name" class="form-control" name="subcategoryname" required>

                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-12">

                            <input type="text" placeholder="Sub-category description" class="form-control" name="subcategorydescription" required>

                        </div>
                    </div>
                    <div class="row mt-4">


                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->