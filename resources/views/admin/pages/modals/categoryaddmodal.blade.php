<div class="modal fade catagoryadd-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Create category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <form method='POST' action="{{ route('admin.pages.adminproductlist.categorycreation') }}" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="row ">
                        <div class="col-md-12">
                            <input type="text" placeholder="category name" class="form-control" name="categoryname" required>

                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-12">

                            <input type="text" placeholder="category description" class="form-control" name="categorydescription" required>

                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <!-- <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile" name="categoryimg"  accept=".jpg,.jpeg,.png">
                            <label class="custom-file-label" for="customFile">Choose image</label>
                        </div> -->
                            <label for="categoryimg">Add Catagory Image</label>
                            <input type="file" name="categoryimg" accept=".jpg,.jpeg,.png" required>
                        </div>

                    </div>




                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>

        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->