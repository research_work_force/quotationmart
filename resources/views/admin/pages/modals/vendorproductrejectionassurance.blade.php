<div class="modal fade productrejection-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">Rejection Reason Field</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="subcategory" method='POST' action="{{ route('admin.pages.adminproductlist.subcategorycreation') }}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <input type="text" placeholder="Reason to reject the product" class="form-control" name="subcategoryname" required>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <input type="hidden" placeholder="id of the product" class="form-control" name="productid" required>
                        </div>
                    </div>
                    <div class="row mt-4">


                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">save</button>
                        
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->