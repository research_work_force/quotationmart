<div class="vertical-menu">

    <div class="h-100">

        <div class="user-wid text-center py-4">
            <div class="user-img">
                <img src="{{asset('assets/images/users/avatar-2.jpg')}}" alt="" class="avatar-md mx-auto rounded-circle">
            </div>

            <div class="mt-3">

                <a href="#" class="text-dark font-weight-medium font-size-16">Customer</a>
                <p class="text-body mt-1 mb-0 font-size-13">Customer Admin</p>

            </div>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <!-- <li class="menu-title">Menu</li> -->

                <li>
                    <a href="{{ route('customer.profile') }}"  class="waves-effect">
                        <i class="mdi mdi-account-star-outline"></i>
                        <span>Profile</span>
                    </a>

                </li>
                <li>
                    <a href="{{ route('customer.order') }}"  class=" waves-effect">
                        <i class="mdi mdi-basket-fill"></i>
                        <span>Order</span>
                    </a>

                    <!-- <ul class="sub-menu" aria-expanded="false">

                        <li><a href="/customerprofilelist">Customer Profile List</a></li>
                        <li><a href="/profileupdateapproval">Profile Update Approval</a></li>
                        <li><a href="javascript: void(0);">Payment Details Approval</a></li>


                    </ul> -->

                </li>
                <li>
                    <a href="{{ route('customer.enquiry') }}"  class=" waves-effect">
                        <i class="mdi mdi-layers-search"></i>
                        <span>Enquiries </span>
                    </a>



                </li>
                <li>
                    <a href="{{ route('customer.featuredvendors') }}"  class="waves-effect">
                        <i class="mdi mdi-email-receive-outline"></i>
                        <span>Inbox </span>
                    </a>



                </li>
                <li>
                    <a href="{{ route('customer.productsearch') }}"  class=" waves-effect">
                      <i class="mdi mdi-movie-search"></i>
                        <span>Product Search </span>
                    </a>



                </li>

                <li>
                    <a href="{{ route('customer.transaction') }}"  class=" waves-effect">
                        <i class="mdi mdi-chart-multiple"></i>
                        <span>Transaction and reports </span>
                    </a>



                </li>

                <li>
                    <a href="{{ route('customer.payment') }}"  class="waves-effect">
                          <i class="mdi mdi-cash-usd-outline"></i>
                        <span>Payment</span>
                    </a>

                </li>
                <li>
                    <a href="{{ route('customer.subscription') }}"  class="waves-effect">
                          <i class="mdi mdi-tag-text"></i>
                        <span>Subscription</span>
                    </a>

                </li>


                <li>
                    <a href="{{ route('customer.featuredvendors') }}" class="waves-effect">
                        <i class="mdi mdi-shield-star"></i>
                        <span>Featured Vendors </span>
                    </a>

                </li>





            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
