@extends('customer.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Customer Enterprise Vendor Profile</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Customer</a></li>
                            <li class="breadcrumb-item active">Customer Enterprise Vendor Profile</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->



                <div class="row">
                    <div class="col-md-12 col-xl-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="profile-widgets py-3">

                                    <div class="text-center">
                                        <div class="">
                                            <img src="assets/images/users/avatar-2.jpg" alt="" class="avatar-lg mx-auto img-thumbnail rounded-circle">
                                            <div class="online-circle"><i class="fas fa-circle text-success"></i></div>
                                        </div>

                                        <div class="mt-3 ">
                                            <a href="#" class="text-dark font-weight-medium font-size-16">Vendor Name</a>
                                            <p class="text-body mt-1 mb-1">@username</p>

                                            <span class="badge badge-success">Trusted By 5000 Customers like you</span>
                                            <span class="badge badge-warning" style="background: linear-gradient(to  right, #12A6CC, #2121bf ) !important;">Enterprise</span>
                                        </div>

                                        <div class="row mt-4  p-3">
                                            <div class="col-md-12">
                                                <h6 class="text-muted">
                                                Vendor Joined Since
                                            </h6>
                                                <h5 class="mb-0">02/06/2019</h5>
                                            </div>
                                        </div>

                                        <div class="row mt-4  p-3">
                                            <div class="col-md-12">
                                              <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                                Contact Vendor
                                            </button>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>




                    </div>

                    <div class="col-md-12 col-xl-9">


                      <div class="card">

                        <img src="{{asset('assets/images/enterprisevendorprofile.jpg')}}" alt="" width="720" height="470" class="mx-auto ">
                      </div>


                    </div>


         </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">List of Products</h1>
                        <!-- <p class="card-title-desc">
                            “It is not the employer who pays the wages. Employers only handle the money. It is the customer who pays the wages.” – Henry Ford
                        </p> -->

                        <div class="row">
                                      <div class="col-md-3 mb-4">
                                        <select class="form-control" name="catagory">
                                          <option value="">catagory 1</option>
                                          <option value="">catagory 2</option>
                                        </select>
                                      </div>
                                      <div class="col-md-3 mb-4">
                                        <select class="form-control" name="catagory">
                                          <option value="">Sub Catagory 1</option>
                                          <option value="">Sub Catagory 2</option>
                                        </select>
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Product Name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                        <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                          <i class="bx bx-loader bx-search font-size-16 align-middle mr-2"></i> Find a Product
                                      </button>
                                      </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->


        <!-- Second Card -->

                <div class="row mt-4">
                    <div class="col-lg-3">
                      <a href="#" >
                      <div class="card">
                          <div class="card-body">


                              <div class="row ">
                                  <div class="col-lg-12" align="center">
                                    <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                                      <div class="mt-4">
                                          <h5>Catagory Name</h5>
                                      </div>
                                  </div>

                              </div>
                          </div>
                      </div>
                    </a>
                  </div>

                  <div class="col-lg-3">
                    <a href="#" >
                    <div class="card">
                        <div class="card-body">


                            <div class="row ">
                                <div class="col-lg-12" align="center">
                                  <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                                    <div class="mt-4">
                                        <h5>Catagory Name</h5>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                  </a>
                </div>

                <div class="col-lg-3">
                  <a href="#" >
                  <div class="card">
                      <div class="card-body">


                          <div class="row ">
                              <div class="col-lg-12" align="center">
                                <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                                  <div class="mt-4">
                                      <h5>Catagory Name</h5>
                                  </div>
                              </div>

                          </div>
                      </div>
                  </div>
                </a>
              </div>

              <div class="col-lg-3">
                <a href="#" >
                <div class="card">
                    <div class="card-body">


                        <div class="row ">
                            <div class="col-lg-12" align="center">
                              <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                                <div class="mt-4">
                                    <h5>Catagory Name</h5>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
              </a>
            </div>

            <div class="col-lg-3">
              <a href="#" >
              <div class="card">
                  <div class="card-body">


                      <div class="row ">
                          <div class="col-lg-12" align="center">
                            <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                              <div class="mt-4">
                                  <h5>Catagory Name</h5>
                              </div>
                          </div>

                      </div>
                  </div>
              </div>
            </a>
          </div>
          <div class="col-lg-3">
            <a href="#" >
            <div class="card">
                <div class="card-body">


                    <div class="row ">
                        <div class="col-lg-12" align="center">
                          <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                            <div class="mt-4">
                                <h5>Catagory Name</h5>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3">
          <a href="#" >
          <div class="card">
              <div class="card-body">


                  <div class="row ">
                      <div class="col-lg-12" align="center">
                        <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                          <div class="mt-4">
                              <h5>Catagory Name</h5>
                          </div>
                      </div>

                  </div>
              </div>
          </div>
        </a>
      </div>
      <div class="col-lg-3">
        <a href="#" >
        <div class="card">
            <div class="card-body">


                <div class="row ">
                    <div class="col-lg-12" align="center">
                      <img src="{{asset('assets/images/small/img-1.jpg')}}" width="200" height="133" alt="" class="">


                        <div class="mt-4">
                            <h5>Catagory Name</h5>
                        </div>
                    </div>

                </div>
            </div>
        </div>
      </a>
    </div>
                </div>


        <!-- Second row end -->

    </div>
    <!-- End Page-content -->








@endsection
