@extends('customer.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Customer Transaction History</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Customer</a></li>
                            <li class="breadcrumb-item active">Customer Transaction History</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="card-title">List of Transaction</h1>
                        <!-- <p class="card-title-desc">
                            “It is not the employer who pays the wages. Employers only handle the money. It is the customer who pays the wages.” – Henry Ford
                        </p> -->

                        <div class="row">
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Invoice No">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Transaction ID">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                          <input class="form-control" type="text" placeholder="Payee Name">
                                      </div>
                                      <div class="col-md-3 mb-4">
                                        <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                                          <i class="bx bx-loader bx-search font-size-16 align-middle mr-2"></i> Find a Transaction
                                      </button>
                                      </div>

                        </div>



                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

        <div class="row">
          <div class="col-4">
              <h4>Total No of Enquiries: 25<h4>
          </div>
          <div class="col-5">
            <h4>  Total Transaction Value: INR 21,01,000.00 <h4>
          </div>

            <div class="col-3">
              <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                Download Transaction History
            </button>
            </div>
        </div>

        <!-- Second Card -->

                <div class="row">
                    <div class="col-lg-12">

                                <div class="table-responsive">
                                    <table class="table table-thinkagainlab mb-0 ">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Invoice No</th>
                                                <th>Order Date </th>
                                                <th>Transaction ID </th>
                                                <th>Transaction Value (INR) </th>
                                                <th>Payee Name</th>
                                                <th>Transaction Type </th>
                                                <th>Payment Mode </th>
                                                <th>Action </th>



                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell </td>

                                                <td>
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                </td>

                                            </tr>

                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell </td>

                                                <td>
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                </td>

                                            </tr>

                                            <tr>
                                                <th scope="row">1</th>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell</td>
                                                <td>Table cell </td>

                                                <td>
                                                  <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" >
                                                    <i class="fas fa-cogs"></i>
                                                </button>

                                                </td>

                                            </tr>








                                        </tbody>
                                    </table>
                                </div>


                    </div>
                </div>
        <!-- Second row end -->

    </div>
    <!-- End Page-content -->








@endsection
