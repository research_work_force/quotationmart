@extends('customer.layouts.loginhome')

@section('content')

<!-- <div class="home-btn d-none d-sm-block">
      <a href="index.html" class="text-dark"><i class="fas fa-home h2"></i></a>
  </div> -->
  <div class="account-pages my-5 pt-sm-5">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-md-8 col-lg-6 col-xl-5">
                  <div class="card overflow-hidden">
                      <div class="bg-login text-center">
                          <div class="bg-login-overlay" style="background: linear-gradient(to  right, #12A6CC, #2121bf ) !important;"></div>
                          <div class="position-relative">
                              <h5 class="text-white font-size-20">Welcome Back !</h5>
                              <p class="text-white-50 mb-0">Sign in to continue to Quotationmart.</p>
                              <a href="index.html" class="logo logo-admin mt-4">
                                  <img src="assets/images/logo-sm-dark.png" alt="" height="30">
                              </a>
                          </div>
                      </div>
                      <div class="card-body pt-5">
                          <div class="p-2">

                              <form class="form-horizontal" method="POST" action="{{ route('customer.registration.store') }}">
                                  {{csrf_field()}}

                                  @if(Session::has('message'))
                                  <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
                                  @endif



                                  <div class="form-group">
                                      <label for="name">Name</label>
                                      <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required>
                                  </div>

                                  <div class="form-group">
                                      <label for="email">Email</label>
                                      <input type="email" class="form-control" id="Email" name="email" placeholder="Enter email" required> 
                                  </div>

                                  <div class="form-group">
                                      <label for="phonenumber">Phone Number</label>
                                      <input type="number" class="form-control" id="phonenumber" name="phonenumber" placeholder="Enter phone number" required>
                                  </div>

                                  <div class="form-group">
                                      <label for="companyname">Company Name</label>
                                      <input type="text" class="form-control" id="companyname" name="companyname" placeholder="Enter company name" required>
                                  </div>

                                  <div class="mt-3">
                                      <button type="submit" class="btn btn-dark btn-technomart btn-block waves-effect waves-light" type="submit">Register</button>
                                  </div>

                                  <div class="mt-4 text-center">
                                      <a href="pages-recoverpw.html" class="text-muted"><i class="mdi mdi-lock mr-1"></i> Forgot your password?</a>
                                  </div>
                              </form>
                          </div>

                      </div>
                  </div>
                  <div class="mt-5 text-center">
                      <!-- <p>Don't have an account ? <a href="pages-register.html" class="font-weight-medium text-primary"> Signup now </a> </p> -->
                      <p>© 2020 Quotationmart. Crafted with <i class="mdi mdi-heart text-danger"></i> by Think Again Lab</p>
                  </div>

              </div>
          </div>
      </div>
  </div>


@endsection
