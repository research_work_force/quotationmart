@extends('customer.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Customer Payment Details</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Customer</a></li>
                            <li class="breadcrumb-item active">Customer Payment Details</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-md-12 col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="profile-widgets py-3">

                            <div class="text-center">
                                <div class="">
                                    <img src="assets/images/users/avatar-2.jpg" alt="" class="avatar-lg mx-auto img-thumbnail rounded-circle">
                                    <div class="online-circle"><i class="fas fa-circle text-success"></i></div>
                                </div>

                                <div class="mt-3 ">
                                    <a href="#" class="text-dark font-weight-medium font-size-16">Customer Name</a>
                                    <p class="text-body mt-1 mb-1">@username</p>

                                    <span class="badge badge-success">Silver</span>
                                    <span class="badge badge-danger">3o days left</span>
                                </div>

                                <div class="row mt-4 border border-left-0 border-right-0 p-3">
                                    <div class="col-md-12">
                                        <h6 class="text-muted">
                                        Contact Joined Since
                                    </h6>
                                        <h5 class="mb-0">02/06/2019</h5>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>




            </div>

            <div class="col-md-12 col-xl-9">


              <div class="card">
                  <div class="card-body">
                      <h5 class="card-title mb-3">Personal Information</h5>

                      <!-- <p class="card-title-desc">
                          Hi I'm Patrick Becker, been industry's standard dummy ultrices Cambridge.
                      </p> -->
                      <div class="row">
                        <div class="col-md-4">

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Name </p>
                              <h6 class="">ABC Pvt. Ltd</h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Designation</p>
                              <h6 class="">Purchase Manager</h6>
                          </div>


                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company GST No</p>
                              <h6 class="">ABC123456789 </h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Pan No</p>
                              <h6 class="">PQRO23456</h6>
                          </div>

                        </div>

                        <div class="col-md-4">




                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Pan Photo</p>
                              <h6 class=""><a href="javascript: void(0);">View Pan Card</a></h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company MSME Registration No</p>
                              <h6 class="">khghgh565ljj</h6>
                          </div>
                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Nature of Company</p>
                              <h6 class="">Botling Manufacturing </h6>
                          </div>
                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Signature photo</p>
                              <h6 class=""><a href="javascript: void(0);">View Signature</a></h6>
                          </div>

                        </div>

                        <div class="col-md-4">



                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Address</p>
                              <h6 class="">2240 Denver Avenue
                                    Los Angeles, CA 90017</h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Visiting Card/ Id Card</p>
                              <h6 class=""><a href="javascript: void(0);">View Visiting ID Card</a></h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Logo</p>
                              <h6 class=""><img src="{{asset('assets/images/logo-dark.png')}}" alt="" width="202" height="40" class="mx-auto "></h6>
                          </div>


                        </div>



                      </div>

                      <div class="row">

                        <div class="col-md-4">
                          <!-- <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                             Update Profile
                        </button> -->

                        </div>
                        <div class="col-md-4">
                          <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                            Renewal Package
                        </button>

                        </div>
                        <div class="col-md-4">


                          <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                            Upgrade Package
                        </button>

                        </div>




                      </div>





                  </div>
              </div>


            </div>


 </div>







                <div class="row">

                  <div class="col-xl-6">
                      <div class="card">
                          <div class="card-body">
                              <h4 class="card-title mb-4">User's Bank Details</h4>

                              <div class="row">
                                  <div class="col-lg-12">


                                      <div class="row">
                                          <div class="col-sm-6">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Account Name</b></p>
                                                  <h5 class="d-inline-block align-middle mb-0">Submitted</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-6">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Account Holder Address</b></p>
                                                  <h5>AHHALD54</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Bank Name</b></p>
                                                  <h5>AHHALD54</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Branch Name</b></p>
                                                  <h5>20/5/2020</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Account No</b></p>
                                                  <h5>20/5/2020</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>IFSC CODE</b></p>
                                                  <h5>20/5/2020</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Bank Address</b></p>
                                                  <h5>20/5/2020</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Swift Code</b></p>
                                                  <h5>20/5/2020</h5>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="mt-4">
                                          <a href="#" class="btn btn-dark btn-technomart btn-sm">Update</a>
                                      </div>
                                  </div>

                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-xl-6">
                      <div class="card">
                          <div class="card-body">
                              <h4 class="card-title mb-4">Revision Box’s Bank Details</h4>

                              <div class="row">
                                  <div class="col-lg-12">


                                      <div class="row">
                                          <div class="col-sm-6">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Account Name</b></p>
                                                  <h5 class="d-inline-block align-middle mb-0">Submitted</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-6">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Account Holder Address</b></p>
                                                  <h5>AHHALD54</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Bank Name</b></p>
                                                  <h5>AHHALD54</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Branch Name</b></p>
                                                  <h5>20/5/2020</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Account No</b></p>
                                                  <h5>20/5/2020</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>IFSC CODE</b></p>
                                                  <h5>20/5/2020</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Bank Address</b></p>
                                                  <h5>20/5/2020</h5>
                                              </div>
                                          </div>
                                          <div class="col-sm-4">
                                              <div class="mt-3">
                                                  <p class="mb-2 text-truncate"><b>Swift Code</b></p>
                                                  <h5>20/5/2020</h5>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="mt-4">
                                          <!-- <a href="#" class="btn btn-dark btn-technomart btn-sm">Update</a> -->
                                      </div>
                                  </div>

                              </div>
                          </div>
                      </div>
                  </div>

                </div>
        <!-- Second row end -->

    </div>
    <!-- End Page-content -->








@endsection
