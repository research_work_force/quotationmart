@extends('customer.layouts.home')

@section('content')

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">Subscription</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Customer</a></li>
                            <li class="breadcrumb-item active">Subscription</li>
                        </ol>
                    </div>

                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- start row -->
        <div class="row">
            <div class="col-md-12 col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="profile-widgets py-3">

                            <div class="text-center">
                                <div class="">
                                    <img src="assets/images/users/avatar-2.jpg" alt="" class="avatar-lg mx-auto img-thumbnail rounded-circle">
                                    <div class="online-circle"><i class="fas fa-circle text-success"></i></div>
                                </div>

                                <div class="mt-3 ">
                                    <a href="#" class="text-dark font-weight-medium font-size-16">Customer Name</a>
                                    <p class="text-body mt-1 mb-1">@username</p>

                                    <span class="badge badge-success">Silver</span>
                                    <span class="badge badge-danger">3o days left</span>
                                </div>

                                <div class="row mt-4 border border-left-0 border-right-0 p-3">
                                    <div class="col-md-12">
                                        <h6 class="text-muted">
                                        Contact Joined Since
                                    </h6>
                                        <h5 class="mb-0">02/06/2019</h5>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>




            </div>

            <div class="col-md-12 col-xl-9">


              <div class="card">
                  <div class="card-body">
                      <h5 class="card-title mb-3">Personal Information</h5>

                      <!-- <p class="card-title-desc">
                          Hi I'm Patrick Becker, been industry's standard dummy ultrices Cambridge.
                      </p> -->
                      <div class="row">
                        <div class="col-md-4">

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Name </p>
                              <h6 class="">ABC Pvt. Ltd</h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Designation</p>
                              <h6 class="">Purchase Manager</h6>
                          </div>


                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company GST No</p>
                              <h6 class="">ABC123456789 </h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Pan No</p>
                              <h6 class="">PQRO23456</h6>
                          </div>

                        </div>

                        <div class="col-md-4">




                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Pan Photo</p>
                              <h6 class=""><a href="javascript: void(0);">View Pan Card</a></h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company MSME Registration No</p>
                              <h6 class="">khghgh565ljj</h6>
                          </div>
                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Nature of Company</p>
                              <h6 class="">Botling Manufacturing </h6>
                          </div>
                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Signature photo</p>
                              <h6 class=""><a href="javascript: void(0);">View Signature</a></h6>
                          </div>

                        </div>

                        <div class="col-md-4">



                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Address</p>
                              <h6 class="">2240 Denver Avenue
                                    Los Angeles, CA 90017</h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Visiting Card/ Id Card</p>
                              <h6 class=""><a href="javascript: void(0);">View Visiting ID Card</a></h6>
                          </div>

                          <div class="mt-3">
                              <p class="font-size-12 text-muted mb-1">Company Logo</p>
                              <h6 class=""><img src="{{asset('assets/images/logo-dark.png')}}" alt="" width="202" height="40" class="mx-auto "></h6>
                          </div>


                        </div>



                      </div>

                      <div class="row">

                        <div class="col-md-4">
                          <!-- <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                             Update Profile
                        </button> -->

                        </div>
                        <div class="col-md-4">
                          <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                            Renewal Package
                        </button>

                        </div>
                        <div class="col-md-4">


                          <button type="button" class="btn btn-dark btn-technomart waves-effect waves-light" style="width:100%;">
                            Upgrade Package
                        </button>

                        </div>




                      </div>





                  </div>
              </div>


            </div>


 </div>

        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="text-center mb-5">
                    <h4>Choose your Subscription Plan</h4>
                    <!-- <p class="text-muted">To achieve this, it would be necessary to have uniform grammar, pronunciation and more common words If several languages coalesce</p> -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card plan-box">
                    <div class="card-body p-4">
                        <div class="media">
                            <div class="media-body">
                                <h5>Silver</h5>
                                <p class="text-muted">Neque quis est</p>
                            </div>
                            <div class="ml-3">
                                <i class="bx bx-walk h1 text-secondary"></i>
                            </div>
                        </div>
                        <div class="py-4 mt-4 text-center bg-soft-light">
                            <h1 class="m-0"><sup><small>₹</small></sup> 1000/ <span class="font-size-13">Per month</span></h1>
                        </div>

                        <div class="plan-features p-4 text-muted mt-2">
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Enquiry for any product</p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Offer Received</p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Negotiation & Order</p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>100% Payment against Proforma Invoice with GST & Freight Charges </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>10 Minimum Enquiries in a Month  </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>24 * 7 Support </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>2 Users [Admin + 1] </p>
                          <p><i class="mdi mdi-backspace-reverse text-danger mr-4"></i>“Letter of Credit” against order value More than 5 Lacs </p>
                          <p><i class="mdi mdi-backspace-reverse text-danger mr-4"></i>“Bank Guarantee” against order value More than 10 Lacs </p>
                          <p><i class="mdi mdi-backspace-reverse text-danger mr-4"></i>Premium Vendors </p>
                          <p><i class="mdi mdi-backspace-reverse text-danger mr-4"></i>Enterprise Vendors </p>
                          <p><i class="mdi mdi-backspace-reverse text-danger mr-4"></i>30/45/60 days Credit against both Buyer & seller mutual agree  </p>
                          <p><i class="mdi mdi-backspace-reverse text-danger mr-4"></i>Customize Payment Option </p>


                        </div>

                        <div class="text-center">
                            <!-- <a href="#" class="btn btn-primary waves-effect waves-light">Sign up Now</a> -->
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card plan-box">
                    <div class="card-body p-4">
                        <div class="media">
                            <div class="media-body">
                                <h5>Gold</h5>
                                <p class="text-muted">Quis autem iure</p>
                            </div>
                            <div class="ml-3">
                                <i class="bx bx-run h1 text-warning"></i>
                            </div>
                        </div>
                        <div class="py-4 mt-4 text-center bg-soft-light">
                            <h1 class="m-0"><sup><small>₹</small></sup> 5000/ <span class="font-size-13">Per month</span></h1>
                        </div>
                        <div class="plan-features p-4 text-muted mt-2">
                            <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Enquiry for any product</p>
                            <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Offer Received</p>
                            <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Negotiation & Order</p>
                            <p><i class="mdi mdi-check-bold text-primary mr-4"></i>100% Payment against Proforma Invoice with GST & Freight Charges </p>
                            <p><i class="mdi mdi-check-bold text-primary mr-4"></i>50 Minimum Enquiries in a Month  </p>
                            <p><i class="mdi mdi-check-bold text-primary mr-4"></i>24 * 7 Support </p>
                            <p><i class="mdi mdi-check-bold text-primary mr-4"></i>5 Users [Admin + 4] </p>
                            <p><i class="mdi mdi-check-bold text-primary mr-4"></i>“Letter of Credit” against order value More than 5 Lacs </p>
                            <p><i class="mdi mdi-check-bold text-primary mr-4"></i>“Bank Guarantee” against order value More than 10 Lacs </p>
                            <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Premium Vendors </p>
                            <p><i class="mdi mdi-backspace-reverse text-danger mr-4"></i>Enterprise Vendors </p>
                            <p><i class="mdi mdi-backspace-reverse text-danger mr-4"></i>30/45/60 days Credit against both Buyer & seller mutual agree  </p>
                            <p><i class="mdi mdi-backspace-reverse text-danger mr-4"></i>Customize Payment Option </p>


                        </div>

                        <div class="text-center">
                            <!-- <a href="#" class="btn btn-primary waves-effect waves-light">Sign up Now</a> -->
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card plan-box">
                    <div class="card-body p-4">
                        <div class="media">
                            <div class="media-body">
                                <h5>Platinum</h5>
                                <p class="text-muted">Sed ut neque unde</p>
                            </div>
                            <div class="ml-3">
                                <i class="bx bx-cycling h1 text-danger"></i>
                            </div>
                        </div>
                        <div class="py-4 mt-4 text-center bg-soft-light">
                            <h1 class="m-0"><sup><small>₹</small></sup> 10000/ <span class="font-size-13">Per month</span></h1>
                        </div>

                        <div class="plan-features p-4 text-muted mt-2">
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Enquiry for any product</p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Offer Received</p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Negotiation & Order</p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>100% Payment against Proforma Invoice with GST & Freight Charges </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Unlimited Enquiries in a Month  </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>24 * 7 Support </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>10 Users [Admin + 9] </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>“Letter of Credit” against order value More than 5 Lacs </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>“Bank Guarantee” against order value More than 10 Lacs </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Premium Vendors </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Enterprise Vendors </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>30/45/60 days Credit against both Buyer & seller mutual agree  </p>
                          <p><i class="mdi mdi-check-bold text-primary mr-4"></i>Customize Payment Option </p>

                        </div>

                        <div class="text-center">
                            <!-- <a href="#" class="btn btn-primary waves-effect waves-light">Sign up Now</a> -->
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- end row -->

    </div>
    <!-- End Page-content -->






@endsection
