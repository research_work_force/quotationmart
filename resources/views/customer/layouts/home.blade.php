<!doctype html>
<html lang="en">

<head>
    <title> Customer Dashboard | Quotationmart</title>

    @include('customer.includes.headerlinks')

</head>

<body data-layout="detached" data-topbar="colored">

    <div class="container-fluid">
        <!-- Begin page -->
        <div id="layout-wrapper">

             @include('customer.includes.navbarheader')
             @include('customer.includes.sidebar')

             @yield('content')

             @include('customer.includes.footer')
         </div>
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

    </div>
    <!-- end container-fluid -->

             @include('customer.includes.rightsidebar')

             @include('customer.includes.footerlinks')


    </body>

</html>
