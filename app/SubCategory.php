<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table='subcategory';
    public function subcategory() {
        return $this->hasOne('App\Category', 'categoryid');
    }
}
