<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempVendorProduct extends Model
{
    //
    protected $table = 'tempvendorproduct';
}
