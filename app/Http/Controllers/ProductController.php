<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductImage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createProduct(Request $request)
    {
        // save categories
        $count = Product::where('productname', '=', $request->input('productname'))->get()->count();
        if($count == 0) {
            $prodrequest = $request->all();
            $product = new Product;
            $product->categoryid = $prodrequest['categoryselector'];
            $product->subcategoryid = $prodrequest['subcategoryselector'];
            $product->productname = strtolower($prodrequest['productname']);
            $product->technicalspecs = strtolower($prodrequest['technicalspecs']);
            $product->productdesc = strtolower($prodrequest['productdesc']);
            $product->hsncode = strtolower($prodrequest['hsncode']);
            $product->productfor = strtolower($prodrequest['productfor']);
            $product->addedby = "me";
            $product->updatedby = null;
            $product->save();
            if($request->hasFile('productimg1')) {
                $productimage = new ProductImage;
                $catimg= $request->file('productimg1');
                $ext = $catimg->getClientOriginalExtension();
                $path='qutationmart/productimg';
                $catimg_filename = strtolower($prodrequest['productname']).'1.'.$ext;
                $catimg->move(public_path($path), $catimg_filename);
                
                if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                    $productimage->image = $catimg_filename;
                    $productimage->createdby = "me";
                    $productimage->updatedby = null;
                    $productimage->productid = $product->id;
                    \Session::flash('message', 'Congratulations!!! Category Created.');
                    \Session::flash('status', 'success');
                   
                }
                else {
                    \Session::flash('message', 'Upload a picture with extension .jpg, .png, .jpeg!');
                    \Session::flash('status', 'danger');
                }
                $productimage->save();
            } 
            
            else {
                \Session::flash('message', 'Have to upload one picture for the product!');
                \Session::flash('status', 'danger');
            }
            if($request->hasFile('productimg2')) {
                $productimage = new ProductImage;
                $catimg= $request->file('productimg2');
                $ext = $catimg->getClientOriginalExtension();
                $path='qutationmart/productimg';
                $catimg_filename = strtolower($prodrequest['productname']).'2.'.$ext;
                $catimg->move(public_path($path), $catimg_filename);
                
                if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                    $productimage->image = $catimg_filename;
                    $productimage->createdby = "me";
                    $productimage->updatedby = null;
                    $productimage->productid = $product->id;
                    \Session::flash('message', 'Congratulations!!! Category Created.');
                    \Session::flash('status', 'success');
                   
                }
                else {
                    \Session::flash('message', 'Upload a picture with extension .jpg, .png, .jpeg!');
                    \Session::flash('status', 'danger');
                }
                $productimage->save();
            }
            if($request->hasFile('productimg3')) {
                $productimage = new ProductImage;
                $catimg= $request->file('productimg3');
                $ext = $catimg->getClientOriginalExtension();
                $path='qutationmart/productimg';
                $catimg_filename = strtolower($prodrequest['productname']).'3.'.$ext;
                $catimg->move(public_path($path), $catimg_filename);
                
                if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                    $productimage->image = $catimg_filename;
                    $productimage->createdby = "me";
                    $productimage->updatedby = null;
                    $productimage->productid = $product->id;
                    \Session::flash('message', 'Congratulations!!! Category Created.');
                    \Session::flash('status', 'success');
                   
                }
                else {
                    \Session::flash('message', 'Upload a picture with extension .jpg, .png, .jpeg!');
                    \Session::flash('status', 'danger');
                }
                $productimage->save();
            }
            if($request->hasFile('productimg4')) {
                $productimage = new ProductImage;
                $catimg= $request->file('productimg4');
                $ext = $catimg->getClientOriginalExtension();
                $path='qutationmart/productimg';
                $catimg_filename = strtolower($prodrequest['productname']).'4.'.$ext;
                $catimg->move(public_path($path), $catimg_filename);
                
                if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                    $productimage->image = $catimg_filename;
                    $productimage->createdby = "me";
                    $productimage->updatedby = null;
                    $productimage->productid = $product->id;
                    \Session::flash('message', 'Congratulations!!! Category Created.');
                    \Session::flash('status', 'success');
                   
                }
                else {
                    \Session::flash('message', 'Upload a picture with extension .jpg, .png, .jpeg!');
                    \Session::flash('status', 'danger');
                }
                $productimage->save();
            }
            \Session::flash('message', 'Congratulations!!! Product is Created.');
            \Session::flash('status', 'success');
        }
        else {
            \Session::flash('message', 'Same product exists!');
            \Session::flash('status', 'danger');
        }
        return redirect()->back();

    }

    public static function fetchProductsinTable()
    {
        $Product = Product::orderBy('categoryid', 'ASC')->orderBy('subcategoryid', 'ASC')->get();

        return $Product;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
