<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomerLead;
use App\CustomerDetails;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

use Mail;



class CustomerLeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $flag=0;

        $customer_name= $request->input('name');
        $customer_email= $request->input('email');
        $customer_phonenumber= $request->input('phonenumber');
        $customer_companyname= $request->input('companyname');

        $token = Str::random(9);
        // $customer_remembertoken=Hash::make($token);
        $customer_remembertoken=$token;



        if( $this->CustomerEmailChecking($customer_email))
        {


             $flag++;
               

        }
        else{

             \Session::flash("message","Email Already Exists");
                \Session::flash("status","danger");

                 return redirect()->back();
               
        }

         if( $this->CustomerCompanyChecking($customer_companyname))
        {

            $flag++;
               

        }
        else{

                 \Session::flash("message","Company Already Exist");
                \Session::flash("status","danger");

                 return redirect()->back();
        }

        if($flag == 2)
        {
             $customerlead= new CustomerLead();
            $customerlead->name=$customer_name;
            $customerlead->mobile=$customer_phonenumber;
            $customerlead->email=$customer_email;
            $customerlead->companyname=$customer_companyname;
            $customerlead->remembertoken=$customer_remembertoken;

          
            
            $customerlead->save();


            //mail to assessor
            $to_name=$customer_name;
            $to_email=$customer_email;

            $data=array('name' => "$to_name", 'remembertoken' => "$customer_remembertoken");

            Mail::send('customer.pages.customerleadmail',$data,function($message) use ($to_name,$to_email){
                             $message->to($to_email)->subject('Quotationmart Customer Account');
            });

              \Session::flash("message","Thank you for registering");
                \Session::flash("status","success");


             return redirect()->back();

        }
        else{

           
                 return redirect()->back();
        }
       

        


       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CustomerEmailChecking($email)
    {
        $email_checking = CustomerDetails::where('email', $email)->first();

        if( empty($email_checking) )

        {
            return 1; 
        }
        else{

            return 0; 
        }
    }

     public function CustomerCompanyChecking($companyname)
    {
        $company_checking = CustomerDetails::where('companyname', $companyname)->first();

        if( empty($company_checking) )

        {
            return 1; 
        }
        else{

            return 0; 
        }
    }

    public function EmailVerify($remembertoken)
    {

            $company_checking = CustomerLead::where('remembertoken', $remembertoken)->first();

            
            print_r($remembertoken);


            die();
    }
}
