<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\SubCategory;
use App\Product;
class SubCategoryController extends Controller
{
    public function createSubCategory(Request $request)
    {
		$count = SubCategory::where('subcategoryname', '=', $request->input('subcategoryname'))->get()->count();
        if($count == 0) {
			$subcategoryDetails= new SubCategory;
			// print_r($request->input('subcategoryname'));
			// die();
			$subcategoryDetails->categoryid=$request->input('categoryid');
			$subcategoryDetails->subcategoryname=strtolower($request->input('subcategoryname'));
			$subcategoryDetails->subcategorydescription=strtolower($request->input('subcategorydescription'));
			$subcategoryDetails->addedby="admin";
			$subcategoryDetails->updatedby=null;
	
			$subcategoryDetails->save();
			\Session::flash('message', 'Congratulations!!! Sub-Category Created.');
			\Session::flash('status', 'success');
		}
		else {
			\Session::flash('message', 'Same sub-category exists!');
            \Session::flash('status', 'danger');
		}

		return redirect()->back();
	}
	

	public function deleteSubCategory(Request $request) {
        $subcategory = SubCategory::where('subcategoryname', '=', $request->input('subcategoryname'))->get()->id;
		$subcategoryid = $subcategory->id;
		$categoryid = $subcategory->categoryid;
        $product = Product::where(['subcategoryid' => $subcategoryid, 'categoryid' => $categoryid])->get()->count();
        if($product == 0) {
            SubCategory::find($subcategoryid)->delete();
        }
        else {
            \Session::flash('message', 'Sub-category exists. Couldn\'t delete');
            \Session::flash('status', 'danger');
        }

	}
	
	public static function fetchSubCategories($categoryid) {
		$SubCategory = SubCategory::where('categoryid', '=', $categoryid)->get()->pluck("subcategoryname", "id");

        return $SubCategory;
	}

	public static function fetchSubCategoriesinTable()
    {
        $SubCategory = SubCategory::orderBy('categoryid', 'ASC')->get();

        return $SubCategory;
    }

	public function getSubCategories($categoryid) {
		$SubCategory = SubCategory::where('categoryid', '=', $categoryid)->get()->pluck("subcategoryname", "id");
        return json_encode($SubCategory);
	}


}
