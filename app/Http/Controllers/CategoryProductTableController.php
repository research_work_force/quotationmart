<!-- erase later: file is expendable -->

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\SubCategory;
use App\Product;

class CategoryProductTableController extends Controller
{
    public static function createTable() {
        $Categories = Category::orderBy('id', 'ASC')->get();
        $SubCategories = SubCategory::orderBy('categoryid', 'ASC')->get();
        $Products = Product::orderBy('categoryid', 'ASC')->orderBy('subcategoryid', 'ASC')->get();
        $data=array('categories'=>$Categories, 'subcategories'=>$SubCategories, 'products'=>$Products);
        return view('admin.pages.categoryproducttable', compact('Categories', 'SubCategories', 'Products'));
    }
}
