<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VendorDetails;

class VendorController extends Controller
{
    
    public function insertVendorInformation()
    {
    	$vendorinfo= new VendorDetails();
    	$vendorinfo->name="John Volcano";
    	$vendorinfo->username="john@".rand(1,999);
    	$vendorinfo->mobile="6053056890";
    	$vendorinfo->email="jon@email.com";
    	$vendorinfo->designation="Manageing director";
    	$vendorinfo->companyname="XYX pvt ltd.";
    	$vendorinfo->companyaddress="kolkata";
    	$vendorinfo->profileimg="img.jpg";
    	$vendorinfo->companylogoimg="logo.jpg";
    	$vendorinfo->companygstno="GST02020230";
    	$vendorinfo->companypanno="PAN202035";
    	$vendorinfo->companypanimg="PAN20305";
    	$vendorinfo->companynature="Very good";
    	$vendorinfo->signatureimg="SIG502151";
    	$vendorinfo->productentry="5";
    	$vendorinfo->password="mcwwwjdoijfiwjfwfjwjfofjwofjwofj";
    	$vendorinfo->sellpromise="1";
    	$vendorinfo->shareinformation="1";
    	$vendorinfo->save();
    	
    }

}
