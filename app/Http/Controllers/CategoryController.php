<?php

// fetch username set usernames
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\SubCategory;
use Exception;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function fetchCategories()
    {
        $Category = Category::get();

        return $Category;
    }

    public static function fetchCategoriesinTable()
    {
        $Category = Category::orderBy('id', 'ASC')->get();

        return $Category;
    }

    // erase later
    public function getCategories()
    {
        $Category = Category::get();

        return $Category;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveCategory(Request $request)
    {
        // save categories
        $count = Category::where('categoryname', '=', $request->input('categoryname'))->get()->count();
        if($count == 0) {
            $category = new Category;
            $category->categoryname = strtolower($request->input('categoryname'));
            $category->categorydescription = strtolower($request->input('categorydescription'));
            $category->addedby = "me";
            $category->updatedby=null;
            if($request->hasFile('categoryimg')) {
                $catimg= $request->file('categoryimg');
                $ext = $catimg->getClientOriginalExtension();
                $path='qutationmart/categoryimages';
                $catimg_filename = $category->categoryname.'.'.$ext;
                $catimg->move(public_path($path), $catimg_filename);
                
                if($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                    $category->categoryimg = $catimg_filename;
                    $category->save();
    
                    \Session::flash('message', 'Congratulations!!! Category Created.');
                    \Session::flash('status', 'success');
                   
                }
                else {
                    \Session::flash('message', 'Upload a picture with extension .jpg, .png, .jpeg!');
                    \Session::flash('status', 'danger');
                }
            } 
            
            else {
                \Session::flash('message', 'Upload a picture for the category!');
                \Session::flash('status', 'danger');
            }
        }
        else {
            \Session::flash('message', 'Same category exists!');
            \Session::flash('status', 'danger');
        }
        return redirect()->back();
    }
    public function deleteCategory(Request $request) {
        $categoryid = Category::where('categoryname', '=', $request->input('categoryname'))->get()->id;
        // $categoryid = $category->id;
        $subcategory = SubCategory::where('categoryid', '=', $categoryid)->get()->count();
        if($subcategory == 0) {
            Category::find($categoryid)->delete();
        }
        else {
            \Session::flash('message', 'Sub-category exists. Couldn\'t delete');
            \Session::flash('status', 'danger');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function fetchCategory($name)
    // {
    //     return Category::find($name);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

// routweb.php
