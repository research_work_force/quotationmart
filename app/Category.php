<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // Table Name
    protected $table = 'category';
    public function category() {
        return $this->hasMany('App\SubCategory', 'id');
    }
}


