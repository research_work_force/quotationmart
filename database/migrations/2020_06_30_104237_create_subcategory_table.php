

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategory', function (Blueprint $table) {

             $table->bigInteger('categoryid')->unsigned();
             $table->foreign('categoryid')->references('id')->on('category')->onDelete('cascade');
             $table->bigIncrements('id');
            
             
             $table->string('subcategoryname',100);
             $table->string('subcategorydescription',200);
             $table->string('addedby',100);
             $table->string('updatedby',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subcategory', function($table)
        {
 
        $table->dropForeign('subcategory_categoryid_foreign');

        $table->foreign('categoryid')->references('id')->on('category');

        });

        Schema::dropIfExists('subcategory');
    }
}
