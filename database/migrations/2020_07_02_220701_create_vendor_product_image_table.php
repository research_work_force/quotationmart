<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorProductImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendorproductimg', function (Blueprint $table) {
            $table->bigInteger('productid')->unsigned();
            $table->foreign('productid')->references('id')->on('vendorproduct')->onDelete('cascade');
            $table->id();
            $table->string('image',200);
            
            $table->string('createdby',200);
            $table->string('updatedby',200)->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

         Schema::table('tempvendorproductimg', function($table)
        {
 
        $table->dropForeign('tempvendorproductimg_productid_foreign');

        $table->foreign('productid')->references('id')->on('tempvendorproduct');

        });
        Schema::dropIfExists('vendorproductimg');
    }
}
