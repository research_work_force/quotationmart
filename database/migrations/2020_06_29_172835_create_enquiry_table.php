<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquirydetails', function (Blueprint $table) {
            $table->id();
            $table->string('enquiryid',500);
            $table->string('customername',500);
            $table->string('companyname',500);
            $table->string('enquirydescription',500);
            $table->string('products',500);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquirydetails'); 
    }
}
