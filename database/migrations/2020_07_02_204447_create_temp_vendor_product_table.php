<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempVendorProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tempvendorproduct', function (Blueprint $table) {
             $table->bigInteger('categoryid')->unsigned();
            $table->foreign('categoryid')->references('id')->on('category')->onDelete('cascade');

            $table->bigInteger('subcategoryid')->unsigned();
            $table->foreign('subcategoryid')->references('id')->on('subcategory')->onDelete('cascade');


            $table->bigIncrements('id');
            $table->string('productname',200);
            
            
             $table->string('technicalspecs',250);
             $table->string('productdesc',500);
             $table->string('hsncode',100);
             $table->string('productfor',200);
             $table->string('countryoforigin',100);
             $table->integer('approvalstatus');
             $table->string('reason',100);
             $table->string('mfddby',100);
             $table->string('addedby',100);
             $table->string('updatedby',100)->nullable();
             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

         Schema::table('tempvendorproduct', function($table)
        {
 
        $table->dropForeign('tempvendorproduct_categoryid_foreign');


        $table->foreign('categoryid')->references('id')->on('category');

        $table->dropForeign('tempvendorproduct_subcategoryid_foreign');
        

        $table->foreign('subcategoryid')->references('id')->on('subcategory');

        });
        Schema::dropIfExists('tempvendorproduct');
    }
}
