<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Customerdetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            


            Schema::create('customerdetails', function(Blueprint $table)
            {
                $table->bigIncrements('id');
                $table->string('name',500);
                $table->string('username',500);
                $table->bigInteger('mobile');
                $table->string('email',500);
                $table->string('designation',500);
                $table->string('companyname',500);
                $table->string('companyaddress',500);
                $table->string('profileimg',500);
                $table->string('companylogoimg',500);
                $table->string('companygstno',500);
                $table->string('companypanno',500);
                $table->string('companypanimg',500);
                $table->string('companynature',500);
                $table->string('signatureimg',500);
                $table->string('password',500);
                $table->integer('finaldecision');
                $table->integer('shareinformation');

               
                $table->timestamps();
            });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customerdetails');
    }
}
