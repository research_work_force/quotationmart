// if not needed, needs to be deleted
$(document).ready(function() {
    $('select[name="categoryselector"]').on('change', function() {
        $categoryid=$(this).val();
        if($categoryid) {
            $.ajax({
                url: '/adminproductlist/getsubcategories/'+$categoryid,
                type:"GET",
                dataType:"json",
                success:function(data) {
                    console.log(data);
                    
                    $('select[name="subcategoryselector"]').empty();

                    $.each(data, function(key, value){
                        $('select[name="subcategoryselector"]').append('<option value="'+ key +'">' + value + '</option>');

                    });
                },
                error: function (e) {
                    console.log(e);
                    
                },
                complete: function (){
                    $('#loader').css("visibility", "hidden");
                }

            });
        }
        else {
            $('select[name="subcategoryselector"]').empty();
        }
    })
})