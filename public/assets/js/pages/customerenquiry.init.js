// // var searchString = document.getElementById('search');
// EnquiryPlacedArray = [20, 34, 27, 59, 37, 26, 38, 25, 37, 26, 38, 25];
// CanceledEnquiryArray = [10, 24, 17, 49, 27, 16, 28, 15, 37, 26, 38, 25];

// Series = {
//     name
// };

// var options = {
//     series: [{
//         name: "Enquiry Placed",
//         type: "line",
//         data: EnquiryPlacedArray
//     },
//     {
//         name: "# of Cancelled Enquiries ",
//         data: CanceledEnquiryArray,
//         type: "area"
//     }],
//     chart: {
//         height: 260,
//         type: "line",
//         toolbar: { show: !1 },
//         zoom: { enabled: !1 }
//     },
//     colors: ["#45cb85", "#3b5de7"],
//     dataLabels: { enabled: !1 },
//     stroke: {
//         curve: "smooth",
//         width: "3",
//         dashArray: [4, 0]
//     },
//     markers: {
//         size: 3
//     },
//     xaxis: {
//         categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]/xaxis.categories,
//         title: {
//             text: "Month"/xaxis.title
//         }
//     },
//     fill: {
//         type: "solid",
//         opacity: [1, .1]
//     },
//     legend: {
//         position: "top",
//         horizontalAlign: "right"
//     }
// };
// (chart = new ApexCharts(document.querySelector("#line-chart-customer-enquiry"), options)).render();
// options = {
//     series: [{
//         name: "Series A"/series[0].name,
//         data: [11, 17, 15, 15, 21, 14]/series[0].data
//     },
//     {
//         name: "Series B"/series[1].name,
//         data: [13, 23, 20, 8, 13, 27]/series[1].data
//     },
//     {
//         name: "Series C"/series[2].name,
//         data: [44, 55, 41, 67, 22, 43]/series[2].data
//     }],
//     chart: {
//         type: "bar",
//         height: 260,
//         stacked: !0,
//         toolbar: { show: !1 },
//         zoom: { enabled: !0 }
//     },
//     plotOptions: {
//         bar: {
//             horizontal: !1,
//             columnWidth: "20%",
//             endingShape: "rounded"
//         }
//     },
//     dataLabels: {
//         enabled: !1
//     },
//     xaxis: {
//         categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]/xaxis.categories
//     },
//     colors: ["#eef3f7", "#ced6f9", "#3b5de7"],
//     fill: { opacity: 1 }
// };
// (chart = new ApexCharts(document.querySelector("#column-chart"), options)).render();
// options = {
//     series: [38, 26, 14]/series,
//     chart: {
//         height: 230,
//         type: "donut"
//     },
//     labels: ["Online", "Offline", "Marketing"],
//     plotOptions: {
//         pie: {
//             donut: {
//                 size: "75%"
//             }
//         }
//     },
//     legend: {
//         show: !1
//     },
//     colors: ["#3b5de7", "#45cb85", "#eeb902"]
// };
// (chart = new ApexCharts(document.querySelector("#donut-chart"), options)).render();
// var chart;
// options = {
//     series: [{
//         name: "Series A"/series[0].name,
//         data: [[2, 5], [7, 2], [4, 3], [5, 2], [6, 1], [1, 3], [2, 7], [8, 0], [9, 8], [6, 0], [10, 1]]/series[0].data
//     },
//     {
//         name: "Series B"/series[1].name,
//         data: [[15, 13], [7, 11], [5, 8], [9, 17], [11, 4], [14, 12], [13, 14], [8, 9], [4, 13], [7, 7], [5, 8], [4, 3]]/series[1].data
//     }],
//     chart: {
//         height: 230,
//         type: "scatter",
//         toolbar: {
//             show: !1
//         },
//         zoom: {
//             enabled: !0,
//             type: "xy"
//         }
//     },
//     colors: ["#3b5de7", "#45cb85"],
//     xaxis: {
//         tickAmount: 10/xaxis.tickAmount
//     },
//     legend: {
//         position: "top"
//     },
//     yaxis: {
//         tickAmount: 7/yaxis.tickAmount
//     }
// };
// (chart = new ApexCharts(document.querySelector("#scatter-chart"), options)).render(),
//     $("#usa-vectormap").vectorMap({
//         map: "us_merc_en",
//         backgroundColor: "transparent",
//         regionStyle: {
//             initial: {
//                 fill: "#556ee6"
//             }
//         },
//         markerStyle: {
//             initial: {
//                 r: 9,
//                 fill: "#556ee6",
//                 "fill-opacity": .9,
//                 stroke: "#fff",
//                 "stroke-width": 7,
//                 "stroke-opacity": .4
//             },
//             hover: {
//                 stroke: "#fff",
//                 "fill-opacity": 1,
//                 "stroke-width": 1.5
//             }
//         }
//     });


// var searchString = document.getElementById('search');
// var datastr = 'answer='+selected + '&section='+data[0].section + '&q_id='+data[0].id+'&attempt_status=1';
$.ajax({
     type:'GET',
     url:'/adminproductlist/getcategories',

     success:function(data) {
        // alert("success");
        console.log("data: ", data);
     },
     error: function(errMsg) {
       console.log(errMsg);
    }
});

var EnquiryPlacedArray = {
    name: "Enquiry Placed",
    data: [20, 34, 27, 59, 37, 26, 38, 25, 37, 26, 38, 25]
};
var CanceledEnquiryArray = {
    name: "# of Cancelled Enquiries ",
    data: [10, 24, 17, 49, 27, 16, 28, 15, 37, 26, 38, 25]
};

var xaxis = {
    categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    title: "Month"
};


var options = {
    series: [{
        name: EnquiryPlacedArray.name,
        type: "line",
        data: EnquiryPlacedArray.data
    },
    {
        name: CanceledEnquiryArray.name,
        data: CanceledEnquiryArray.data,
        type: "area"
    }],
    chart: {
        height: 260,
        type: "line",
        toolbar: { show: !1 },
        zoom: { enabled: !1 }
    },
    colors: ["#45cb85", "#3b5de7"],
    dataLabels: { enabled: !1 },
    stroke: {
        curve: "smooth",
        width: "3",
        dashArray: [4, 0]
    },
    markers: {
        size: 3
    },
    xaxis: {
        categories: xaxis.categories,
        title: {
            text: xaxis.title
        }
    },
    fill: {
        type: "solid",
        opacity: [1, .1]
    },
    legend: {
        position: "top",
        horizontalAlign: "right"
    }
};

var series = [
    {
        name: "Series A",
        data: [11, 17, 15, 15, 21, 14]
    },
    {
        name: "Series B",
        data: [13, 23, 20, 8, 13, 27]
    },
    {
        name: "Series C",
        data: [44, 55, 41, 67, 22, 43]
    },
];

var xaxis = {
    categorise: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
};

(chart = new ApexCharts(document.querySelector("#line-chart-customer-enquiry"), options)).render();
options = {
    series: [{
        name: series[0].name,
        data: series[0].data
    },
    {
        name: series[1].name,
        data: series[1].data
    },
    {
        name: series[2].name,
        data: series[2].data
    }],
    chart: {
        type: "bar",
        height: 260,
        stacked: !0,
        toolbar: { show: !1 },
        zoom: { enabled: !0 }
    },
    plotOptions: {
        bar: {
            horizontal: !1,
            columnWidth: "20%",
            endingShape: "rounded"
        }
    },
    dataLabels: {
        enabled: !1
    },
    xaxis: {
        categories: xaxis.categories
    },
    colors: ["#eef3f7", "#ced6f9", "#3b5de7"],
    fill: { opacity: 1 }
};
var series = [38, 26, 14];
(chart = new ApexCharts(document.querySelector("#column-chart"), options)).render();
options = {
    series: series,
    chart: {
        height: 230,
        type: "donut"
    },
    labels: ["Online", "Offline", "Marketing"],
    plotOptions: {
        pie: {
            donut: {
                size: "75%"
            }
        }
    },
    legend: {
        show: !1
    },
    colors: ["#3b5de7", "#45cb85", "#eeb902"]
};
(chart = new ApexCharts(document.querySelector("#donut-chart"), options)).render();
var chart;
var series = [
    {
        name: "Series A",
        data: [[2, 5], [7, 2], [4, 3], [5, 2], [6, 1], [1, 3], [2, 7], [8, 0], [9, 8], [6, 0], [10, 1]]
    },
    {
        name: "Series B",
        data: [[15, 13], [7, 11], [5, 8], [9, 17], [11, 4], [14, 12], [13, 14], [8, 9], [4, 13], [7, 7], [5, 8], [4, 3]]
    }
];
var xaxis = {
    tickAmount: 10
};
var yaxis = {
    tickAmount: 7
};
options = {
    series: [{
        name: series[0].name,
        data: series[0].data
    },
    {
        name: series[1].name,
        data: series[1].data
    }],
    chart: {
        height: 230,
        type: "scatter",
        toolbar: {
            show: !1
        },
        zoom: {
            enabled: !0,
            type: "xy"
        }
    },
    colors: ["#3b5de7", "#45cb85"],
    xaxis: {
        tickAmount: xaxis.tickAmount
    },
    legend: {
        position: "top"
    },
    yaxis: {
        tickAmount: yaxis.tickAmount
    }
};
(chart = new ApexCharts(document.querySelector("#scatter-chart"), options)).render(),
    $("#usa-vectormap").vectorMap({
        map: "us_merc_en",
        backgroundColor: "transparent",
        regionStyle: {
            initial: {
                fill: "#556ee6"
            }
        },
        markerStyle: {
            initial: {
                r: 9,
                fill: "#556ee6",
                "fill-opacity": .9,
                stroke: "#fff",
                "stroke-width": 7,
                "stroke-opacity": .4
            },
            hover: {
                stroke: "#fff",
                "fill-opacity": 1,
                "stroke-width": 1.5
            }
        }
    });
